# MC-1000 System Software #
Software de apoio, device drivers, patches para firmwares e correlatos para este pequeno bastardo inglório da computação brazuca, o [MC-1000 da CCE](https://sites.google.com/site/ccemc1000/).

## Conteúdo ###
Este repositório está dividido em duas seções principais, a saber:

* [Documentação](docs) - cada seção tem sua documentação, mas aqui se encontram as de escopo geral.
* [Firmware](Firmware/README.md) - Hacks e patches para o Firmware do aparelho (original e BD.OS)
* [Hardware](Hardware/README.md) - Device Drivers, hacks, mods e utilitários de suporte.

Cada seção contêm programas dividos em categorias, que por sua vez possuem a seguinte estrutura:

* Diretório de binários, _**bin**_, onde os programas pré-montados/compilados podem ser baixados para uso imediato. Normalmente o programa é apresentado em 3 formatos:
	* _**.BAS**_ para ser copiado num pendrive/SD do BlueDrive;
	* _**.wav**_ para ser carregado pela porta cassete;
	* _**.cas**_ para ser usado em emuladores.
* Diretório de documentação, _**docs**_, onde a documentação de cada programa (quando existente) será encontrada.
* Diretório de fontes, _**souces**_, com o fonte dos programas.

Leia a documentação de cada seção/categoria para mais detalhes.

## Pré-requisitos ###
* Conhecimentos em:
	* Assembly Z80
	* Eletrônica Digital
	* Arquitetura do MC-1000
* Para o build do código
	* Gnu Make
		* Se usuário de Windows, ter instalado ao menos uma das opções abaixo:
			* coreutils da Gnu
			* Cygwin
			* MinGW
* Para o build do código 
	* [MC-1000 Cas Tools](https://github.com/ensjo/MC1000CasTools)
	* [SjASM++](https://bitbucket.org/net-lisias-retro/sjasmpp)
		* O SjASMPlus não é mais oficialmente suportado (deve funcionar, mas eu não verifico mais).
* Para o deploy do código
	* Media Player ou Sound Editor para carregar os programas pela porta cassete do MC-1000
	* BlueDrive recomendada, mas não exigida (exceto quando explicitamente mencionada).
	* Um MC-1000 funcionando. :-)
		* Stock, 16 Kbytes
		* Alguns poucos exigem expansão de memória e/ou BlueDrive.
		* Na ausência de informação em contrário, o alvo é o stock 16Kb.

## Known issues ##

* O SjASM e o SjASMPlus possuem um bug chato que emite falsos erros ("Bytes Lost" e "change size"), o que faz o Gnu Make parar o *building*.
	* Dar o make novamente recomeça o processo de onde parou, pois a emissão dos erros não interrompe a montagem e o arquivo binário é gerado. Um saco, mas dá pra levar.
	* Esta é uma das principais razões de eu ter forkado o projeto e criado o [SjASM++](https://bitbucket.org/net-lisias-retro/sjasmpp) e a razão deste fork ser o recomendado para este projeto (estes são os primeiros bugs a serem resolvidos, de certeza).

## Contatos e links ###

* Lisias 
	* ***_retro at lisias dot net_*** para contatos pessoais, sugestões, oferecimento de ajuda e papo furado. :-)
	* ***_support at lisias dot net_*** para suporte e relatório de erros. Adicionne `MC-1000` e a URL do componente no assunto do email, por favor.
* Comunidade no [Google Plus](https://plus.google.com/u/0/communities/105457203690711411346).
* Grupo no [Facebook](https://www.facebook.com/groups/mc1000/?fref=nf)
* Sítio do [CCE MC-1000](https://sites.google.com/site/ccemc1000/)
* Artigos do MC-1000 no [Locutório do ENSJO](http://ensjo.blogspot.com.br/search/label/MC-1000?view=classic)

Por favor não peça suporte ou relate erros nas minhas contas pessoais, tampouco use o email de suporte para assuntos pessoais. Existe uma razão para eu ter criado duas contas diferentes. ;-)

Por favor compreenda que contactar me sobre este projeto usando qualquer outra forma de contato será solenemente ignorada.
