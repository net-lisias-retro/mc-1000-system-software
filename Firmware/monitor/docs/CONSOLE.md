# Firmware::Monitor::CONSOLE #

## Descrição ##
Algumas rotinas da ROM precisam ser atualizadas para usar as rotinas substitutas.

No momento, as novas rotinas estão 40 bytes menores, de forma que a atual implementação é superavitária.

### AsmDoc ###

	; =====
	; KEY: Entrada console. Aguarda até que uma tecla seja pressionada e então liberada.
	; Destrói:	AF 
	; Retorno:
	; 		Se sim, A e KEYSW (#012E) recebem #FF, E o código da tecla é colocado em KEY0+1 (#011C).
	; 		Se não, A e KEYSW (#012E) recebem #00.
	;		F(Z) retorna atualizado com o valor de A
	;		IY retorna apontando para o buffer de teclas configurado para a SKEY?
	
	; =====
	; WAIT_FOR_KEY: Há uma tecla pressionada? Se estiver, espera até que seja liberada.
	; TODO: Capar fora isto aqui. Além de gastar espaço, ela atrapalha joguinhos em BASIC pois tráva o interpetrador inteiro.
	; Implementar uma enventual tráva por teclado pelo CTRL-S, que é o que todo mundo faz.
	; Destrói:	Nada
	; Retorno:	Nenhum
	
	; =====
	; XKEY? : Zera a tecla corrente (se existir) e lê uma nova. (INKEY$)
	; Destrói:	AF, IY
	; Retorno:
	; 		Se sim, A e KEYSW (#012E) recebem #FF, E o código da tecla é colocado em KEY0+1 (#011C).
	; 		Se não, A e KEYSW (#012E) recebem #00.
	;		F(Z) retorna atualizado com o valor de A
	;		IY retorna apontando para o buffer de teclas configurado para a SKEY?
	
	; =====
	; KEY?: Esta rotina Verificava se uma mesma tecla está pressionada em um intervalo de 7ms.
	; Contudo, a própria SKEY? passou a fazer leituras já com debounce, de forma que esta rotina
	; perdeu sua função original. A presente implementação mimetiza o comportamento da anterior
	; para preservar a interface de programação.
	; Destrói:	AF, IY
	; Retorno:
	; 		Se sim, A e KEYSW (#012E) recebem #FF, E o código da tecla é colocado em KEY0+1 (#011C).
	; 		Se não, A e KEYSW (#012E) recebem #00.
	;		F(Z) retorna atualizado com o valor de A
	;		IY retorna apontando para o buffer de teclas configurado para a SKEY?

### Status ###

* Work in progress *

### *Known Issues* ###

*TODO*

## Instruções ##

Nada de especial: carregar, rodar e seguir as instruções da tela.

## Fonte Comentado ##

Arquivos fontes da solução:

*TODO*

### Arquivo principal ###

*TODO*
