# Firmware::Monitor::MSG #

## Descrição ##
Algumas rotinas da ROM precisam ser atualizadas para usar as rotinas substitutas.

### AsmDoc ###
	; ==========
	; Módulo MSG: Imprime em COUT Strings de diversos formatos.
	
	; =====
	; .C: Imprime uma CString (String terminada em #00).
	: Entrada:	HL apontando para String
	; Destrói:	AF, HL
	
	; =====
	; .PAS: Imprime uma String Pascal (String iniciada com LEN(str), podendo conter #00).
	: Entrada:	HL apontando para String
	; Destrói:	AF, HL
	
	; =====
	; .PAS: Imprime uma String de tamanho arbitrário.
	: Entrada:
	;			HL apontando para String
	;			B tamanho da String. Se zero, imprime 256 caracteres.
	; Destrói:	AF, HL, B

### Status ###

Implementado e testado

### *Known Issues* ###

Nenhuma até o momento.

## Instruções ##

Nada de especial: carregar, rodar e seguir as instruções da tela.

## Fonte Comentado ##

Arquivos fontes da solução:

*TODO*

### Arquivo principal ###

*TODO*
