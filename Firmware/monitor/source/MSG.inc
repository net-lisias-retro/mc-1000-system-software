MSG_TEST_BED_START		EQU		$

; ==========
; M�dulo MSG: Imprime em COUT Strings de diversos formatos.

	MODULE	MSG	
; =====
; .C: Imprime uma CString (String terminada em #00).
; Entrada:	HL apontando para String
; Destr�i:	AF, HL
C:
1:				LD		A, (HL)
				OR		A
				RET		Z
				LD		C, A
				CALL	_COUT
				INC		HL
				JR		1b

; =====
; .PAS: Imprime uma String Pascal (String iniciada com LEN(str), podendo conter #00).
; Entrada:	HL apontando para String
; Destr�i:	AF, HL
PAS:			PUSH	BC
				LD		A, (HL)
				LD		B, A
				INC		HL
				CALL	CHARS
				POP		BC
				RET

; =====
; .PAS: Imprime uma String de tamanho arbitr�rio.
; Entrada:
;			HL apontando para String
;			B tamanho da String. Se zero, imprime 256 caracteres.
; Destr�i:	AF, HL, B
CHARS:		
1:				LD		C, (HL)
				CALL	_COUT
				INC		HL
				DJNZ	1b
				RET
	ENDMODULE
	
MSG_ORG_SIZE			EQU		$C0E9 - $C0DE 	 		; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
														; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
														; (tem margem pra isso, mas d� mais trabalho!)
MSG_TEST_BED_END		EQU		$
MSG_TEST_BED_SIZE		EQU		MSG_TEST_BED_END - MSG_TEST_BED_START
	DISPLAY	"new size ",/H,MSG_TEST_BED_SIZE
	DISPLAY	"org size ",/H,MSG_ORG_SIZE
	DISPLAY "MSG diferen�a ",/D,(MSG_ORG_SIZE - MSG_TEST_BED_SIZE)
	