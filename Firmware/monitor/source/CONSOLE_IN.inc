; ************
; Rotinas de Entrada de Console do Firmware que precisaram ser adaptadas.

; =====
; KEY: Entrada console. Aguarda at� que uma tecla seja pressionada e ent�o liberada.
; Destr�i:	AF, IY, IX 
; Retorno:
; 		Se sim, A e KEYSW (#012E) recebem #FF, E o c�digo da tecla � colocado em KEY0+1 (#011C).
; 		Se n�o, A e KEYSW (#012E) recebem #00.
;		F(Z) retorna atualizado com o valor de A
;		IY retorna apontando para o buffer de teclas configurado para a SKEY?
KEY:
				LD		A, (KEYSW)
				OR		A
				JR		Z, 2f
				LD		A, (HEAD)		; Checa modo BASIC/GAME
				INC		A
				JR		NZ, 1f			; Se n�o t� em modo BASIC, d� um BEEP.
				CALL	_BEEP			; WHY IN HELL? FIXME: Get rid of this, perhaps move it some levels up!
1:				LD		A, (KEY0+1)
				JR		WAIT_FOR_KEY

2:				CALL	KEY?
				JR		KEY

; =====
; WAIT_FOR_KEY: H� uma tecla pressionada? Se estiver, espera at� que seja liberada.
; TODO: Capar fora isto aqui. Al�m de gastar espa�o, ela atrapalha joguinhos em BASIC pois tr�va o interpetrador inteiro.
; Implementar uma enventual tr�va por teclado pelo CTRL-S, que � o que todo mundo faz.
; Destr�i:	IY, IX
; Retorno:	Nenhum
WAIT_FOR_KEY:
				PUSH	AF
.loop:			XOR		A
				LD		(KEYSW), A
				CALL	KEY?
				JR		NZ, .loop
				POP		AF
				RET

; =====
; XKEY? : Zera a tecla corrente (se existir) e l� uma nova. (INKEY$)
; Destr�i:	AF, IY, IX
; Retorno:
; 		Se sim, A e KEYSW (#012E) recebem #FF, E o c�digo da tecla � colocado em KEY0+1 (#011C).
; 		Se n�o, A e KEYSW (#012E) recebem #00.
;		F(Z) retorna atualizado com o valor de A
;		IY retorna apontando para o buffer de teclas configurado para a SKEY?
XKEY?:			
				XOR		A
				LD		(KEYSW), A
; =====
; KEY?: Esta rotina verificava se uma mesma tecla est� pressionada em um intervalo de 7ms.
; Contudo, a pr�pria SKEY? passou a fazer leituras j� com debounce, de forma que esta rotina
; perdeu sua fun��o original. A presente implementa��o mimetiza o comportamento da anterior
; para preservar a interface de programa��o.
; Destr�i:	AF, IY, IX
; Retorno:
; 		Se sim, A e KEYSW (#012E) recebem #FF, E o c�digo da tecla � colocado em KEY0+1 (#011C).
; 		Se n�o, A e KEYSW (#012E) recebem #00.
;		F(Z) retorna atualizado com o valor de A
;		IY retorna apontando para o buffer de teclas configurado para a SKEY?
KEY?:
				PUSH	DE
				LD		A, (KEYSW)
				OR		A
				JR		NZ, .exit
				CALL	KEYBRD.CIN?
				JR		Z, .newval
				LD		A, (IY)						; IY retorna de SKEY?.CALL com o ponteiro para o buffer de teclas.
				LD		(KEY0+1), A
				LD		A, $FF
.newval:		LD		(KEYSW), A
.exit:			POP		DE
				RET

@CONSOLE_IN_ORG_SIZE			EQU		$C37F - $C31B