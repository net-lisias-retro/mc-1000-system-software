MONITOR_CPHLDE_TEST_BED_START		EQU		$

CP_HLDE:		; Simula "CP HL,DE". (era RHLDE)
				; {CPREG} <CompareHLDE> [CPDEHL]
				OR		A		; Apenas reseta o C Flag
				SBC		HL, DE	; Subtrai, setando flags
				ADD		HL, DE	; Adiciona de volta, setando flags
								; ADD seta C, mas preserva S e Z que � o que usamos para comparar.
								; Nice trick from http://z80-heaven.wikidot.com/optimization
				RET

MONITOR_CPHLDE_ORG_SIZE					EQU		$DC11 - $DC0B 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
																; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
																; (tem margem pra isso, mas d� mais trabalho!)
MONITOR_CPHLDE_TEST_BED_END				EQU		$
MONITOR_CPHLDE_TEST_BED_SIZE			EQU		MONITOR_CPHLDE_TEST_BED_END - MONITOR_CPHLDE_TEST_BED_START
	DISPLAY	"new size ",/H,MONITOR_CPHLDE_TEST_BED_SIZE
	DISPLAY	"org size ",/H,MONITOR_CPHLDE_ORG_SIZE
	DISPLAY "CP_HLDE diferen�a ",/D,(MONITOR_CPHLDE_ORG_SIZE - MONITOR_CPHLDE_TEST_BED_SIZE)
				