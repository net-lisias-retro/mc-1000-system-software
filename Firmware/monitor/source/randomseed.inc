MONITOR_RANDOMSEED_TEST_BED_START		EQU		$

; ======
; RANDOMSEED: Atualiza n�mero aleat�rio.
;
; Destr�i HL, DE, F
RANDOMSEED:
					LD      HL, (RANDOM)
					LD      DE, $B2E7
					ADD     HL, DE
					LD      (RANDOM), HL
					RET

MONITOR_RANDOMSEED_ORG_SIZE					EQU		0 		 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
														; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
														; (tem margem pra isso, mas d� mais trabalho!)
MONITOR_RANDOMSEED_TEST_BED_END				EQU		$
MONITOR_RANDOMSEED_TEST_BED_SIZE			EQU		MONITOR_RANDOMSEED_TEST_BED_END - MONITOR_RANDOMSEED_TEST_BED_START
	DISPLAY	"new size ",/H,MONITOR_RANDOMSEED_TEST_BED_SIZE
	DISPLAY	"org size ",/H,MONITOR_RANDOMSEED_ORG_SIZE
	DISPLAY "RANDOMSEED diferen�a ",/D,(MONITOR_RANDOMSEED_ORG_SIZE - MONITOR_RANDOMSEED_TEST_BED_SIZE)
