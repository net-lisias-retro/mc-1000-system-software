	OUTPUT	bin/MSG.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"
	
FILEHEADER:
.filename:	DB		"MSG  "
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			
	; Mensagem 
			LD		HL, str_mensagem
			CALL	SYS.MSG

			LD		HL, str_c
			CALL	MSG.C
			LD		HL, str_pas
			CALL	MSG.PAS
			
			RET
			
;						-12345678901234567890123456789012
str_mensagem:  		DB	"TESTE DAS ROTINAS DO MONITOR QUE"
					DB	"PRECISAM SER ALTERADAS DEVIDO AS"
					DB	"NOVAS ROTINAS."
str_newline:		DB	"\R\N",ASCII.NUL

str_c				DB	"TESTE DE MSG.C\R\N",ASCII.NUL
str_pas:
.size				DB	.end - $
.data				DB	"TESTE DE MSG.PAS\R\N"
.end				EQU	$

	INCLUDE		mc1000_pvt.def
	INCLUDE		MSG.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
