; ======
; FAST Integer Maths
; http://sgate.emt.bme.hu/patai/publications/z80guide/part4.html


	MODULE FMATH

; Multiplication
MPY:

; ======
; Performs the operation HL=H*E
; Destr�i:
;		F, HL, DE
; Retorno:
;		HL: Resultado
;		C: Se 1, houve overflow
.DxE:
					LD		D, 0		; clearing D and L
					LD		L, D
					LD		B, 8		; we have 8 bits to handle
1:
					ADD		HL, HL		; advancing a bit
					JP		NC, 2f		; if zero, we skip the addition (jp is used for speed)
					ADD		HL, DE		; adding to the product if necessary
2:
					DJNZ	1b
					RET

; ======
; Performs the operation HL=DE*A
; Destr�i: TODO
; Retorno: TODO
.DExA:
					LD		HL, 0		; HL is used to accumulate the result
					LD		B, 8		; the multiplier (A) is 8 bits wide
1:
					RRCA				; putting the next bit into the carry
					JP		NC, 2f		; if zero, we skip the addition (jp is used for speed)
					ADD		HL, DE		; adding to the product if necessary
2:
					SLA		E			; calculating the next auxiliary product by shifting
					RL		D			; DE one bit leftwards (refer to the shift instructions!)
					DJNZ	1b
					RET

; ======
; Performs the operation DEHL=DE*BC
; Destr�i: TODO
; Retorno: TODO
.BCxDE:
					LD		HL, 0
					LD		A, 16
1:
					ADD		HL, HL
					RL		E
					RL		E
					JP		NC, 2f
					ADD		HL, BC
					JP		NC, 2f
					INC		DE			; This instruction (with the jump) is like an "ADC DE,0"
2:
					DEC		A
					JP		NZ, 1b
					RET
; Division
DIV:

; ======
; Performs the operation HL=HL/D
; Destr�i: TODO
; Retorno: TODO
HL_D:
					XOR		A			; clearing the upper 8 bits of AHL
					LD		B, 16		; the length of the dividend (16 bits)
1:
					ADD		HL,HL		; advancing a bit
					RLA
					CP		D			; checking if the divisor divides the digits chosen (in A)
					JP		C, 2f		; if not, advancing without subtraction
					SUB		D			; subtracting the divisor
					INC		L			; and setting the next digit of the quotient
2:
					DJNZ	1b
					RET


	ENDMODULE
