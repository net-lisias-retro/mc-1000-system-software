# Firmware::BASIC::GETLIN #

## Descrição ##
*TODO*

### AsmDoc ###
	*TODO*

### Status ###

*WORK IN PROGRESS*. Não foi nem testado ainda!

### *Known Issues* ###

*TODO*

## Instruções ##

*Work in Progress*

| Key        | Status | Functiom
------------------:|:-------------:|:-----------------------------
Ctrl+C (ETX)       | Legacy        | Cancela a linha, com aviso para cancelar o AUTO se ativo
Ctrl+, (RS)        | Remapped      | Limpa a tela, move cursor para o topo, (a definir: o que acontece com o buffer?). Era acessado por Ctrl+^.
Enter, Ctrl+M (CR) | Legacy        | Encerra a edição, pula para próxima linha
Ctrl+U (SI)        | Legacy        | Restarta a Edição (imprime "\" no ponto do cursor para sinalizar)
Ctrl+X (ETB)       | New           | Restarta a Edição (imprime "\" no ponto do cursor para sinalizar)
RUBOUT (DEL)       | New, Remapped | Apaga o caractere à esquerda, desloca buffer.
Ctrl+RUBOUT (RUB)  | New           | Apaga o caractere sob o cursor, desloca buffer.
Shft+RUBOUT (INS)  | New           | Insere um espaço sob o cursor, deslocando buffer.
Ctrl+H (BS)        | Legacy        | Retrocede o cursor um caractere à esquerda
Ctrl+L (FF)        | Legacy        | Avança o cursor um caractere à direita
Ctrl+J (LF)        | Legacy        | Abaixa o cursor uma linha, se houver digitação suficiente
Ctrl+K (VT)        | Legacy        | Sobe o cursor uma linha, se houver digitação suficiente 

## Fonte Comentado ##

Arquivos fontes da solução:

* [Arquivo principal](../source/GELIN.asm)
* [Rotina GETLIN](../source/GETLIN.inc)

### Arquivo principal ###

*TODO*

### GETLIN ###

*TODO*

## Histórico ##

Durante os *brainstormings* sobre um novo firmware para o BlueDrive do MC-1000 (ver [aqui](https://docs.google.com/document/d/1lE8UUJbeeELHQdDuEFQ91SMQBU89wqB1dXI4H9zsLOw/edit?usp=sharing)), *TODO*


