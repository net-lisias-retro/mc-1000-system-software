GETLIN_TEST_BED_START		EQU		$

; =====
; GETL: Entra uma linha do teclado.
; {INPLOC} <InputLine> [RINPUT]
GETL:
.CURRENT_SIZE	EQU		V_GETLIN.data+0		; Current buffer size, plus 1 
.BUFFER			EQU		V_GETLIN.data+1		; Endere�o do Buffer para armazenar os caracteres. Default INPBUF. 2 bytes
.FREE_2			EQU		V_GETLIN.data+2		; Free Space. 2 bytes.

.INIT:
				CALL	CON.INIT
				
.RESET:			LD		A, Z80.OP.JP
				LD		(V_GETLIN.code), A
				LD		HL, .GET
				LD		(V_GETLIN.code+1), HL
				LD		HL, INPBUF
				LD		(.BUFFER), HL
				RET
				
.FN_CANCEL:		; Trata Ctrl+C: cancela entrada.
				XOR		A					; Adicionalmente, desativa AUTO.
				LD		(AUTOFG), A
				CALL	_PRINTCRLF
				SCF							; Sinaliza linha cancelada.
				RET

; =====
; Retorna
;		F(C) = 1 se linha abortada por CTRL+C
.GET:
				LD		HL, (.BUFFER)
				LD		B, #01
				LD		A, B
				LD		(.CURRENT_SIZE), A

.loop:			LD		A, (.CURRENT_SIZE)
; La�o da rotina GETL.
				CP		B
				JR		NC, 1f
				LD		A, B
				LD		(.CURRENT_SIZE), A
				
1:				CALL	CON.KEY
				
				CP		ASCII.ETX			; Ctrl+C
				JR		Z, .FN_CANCEL

				CP		ASCII.RS
				JR		NZ, 1f
				LD		A, ASCII.SUB		; limpa a tela
				CALL	_PRINTAPOS
				LD		A, ASCII.RS			; cursor no in�cio da tela
				JR		.printAndNext
		
1:				CP		ASCII.CR
				JR		NZ, 1f
				LD		(HL), #00			; Finaliza a linha
				JP		_PRINTCRLF			; D� ENTER e retorna
				
1:				CP		ASCII.SI			; Ctrl+U
				JR		Z, 2f
1:				CP		ASCII.ETB			; Ctrl+X (era @)
				JR		NZ, 1f
2:				LD		A, "\\"				; reinicia
				CALL	_PRINTAPOS
				CALL	_PRINTCRLF
				JR		.GET
				
1:				CP		ASCII.DEL			; DELETE
				JR		NZ, 1f
				CALL	.FN_DEL
				JR		C, .loop
				
1:				CP		EXASC.RUB			; RUBOUT
				JR		NZ, 1f
				CALL	.FN_RUB
				
1:				CP		EXASC.INS			; INSERT
				JR		NZ, 1f
				CALL	.FN_INS
				
1:				CP		ASCII.BS			; Ctrl+H
				JR		NZ, 1f
				CALL	.FN_BS				; Retrocede cursor
				JR		C, .loop
				
1:				CP		ASCII.FF			; Ctrl+L
				JR		Z, .DB3C 			; avan�a cursor
				
				CP		ASCII.LF			; Ctrl+J
				JR		Z, .DB4C 			; baixa cursor
				
				CP		ASCII.VT			; Ctrl+K
				JR		Z, .DB7A 			; sobe cursor
				
				LD		C, A				; N�o deixa estourar limite de 255 caracteres
				LD		A, B
				CP		#FF
				LD		A, C
				JR		NC, .loop
				
				CALL	.DBC4
				OR		A
				JR		NZ, 1F
				LD		A, "8"				; TODO: Por que "8"
1:				LD		(HL), A
				LD		(IPHFLG), A

.DB28:			INC		HL
				INC		B

.printAndNext:	CALL	_PRINTAPOS
				JR		.loop

				LD		A, #01
				LD		(NORMAL), A
				POP		DE
				JP		.loop

; Ctrl+L: Avan�a cursor no buffer.
.DB3C:			LD		C, A
				LD		A, (.CURRENT_SIZE)
				DEC		A
				CP		B
				LD		A, C
				CALL	C, _BEEP
				JP		C, .loop
				JR		.DB28

; Ctrl+J: Baixa cursor na tela, se houver algo j� digitado na linha de baixo.
.DB4C:			PUSH	AF
; Obt�m a largura da tela.
				LD		A, (DLNG)
; Se n�o houver linha digitada abaixo, emite bip.
				ADD		A, B
				LD		C, A
				LD		A, (.CURRENT_SIZE)
				CP		C
				CALL	C, _BEEP
				JR		C, .DB76
; Atualiza contador.
				LD		B, C
; Baixa cursor na tela.
				POP		AF
				LD		C, A
				CALL	_COUT
; Atualiza POS().
				LD		A, (CURPOS)
				LD		C, A
				LD		A, (DLNG)
				ADD		A, C
				LD		(CURPOS), A
; Atualiza ponteiro.
				LD		DE, (DLNG)
				ADD		HL, DE
				DB		%00111110	; Instru��o LD A, nn : 00 111 110 - oculta a pr�xima instru��o sacrificando o A
.DB76:			POP		AF
				JP		.loop

; Ctrl+K: Sobe cursor na tela, se houver algo j� digitado na linha de cima.
.DB7A:			PUSH	AF
; Obt�m a largura da tela.
				LD		A, (DLNG)
; Se n�o houver linha digitada acima, emite bip.
				LD		C, A
				LD		A, B
				SUB		C
				CALL	C, _BEEP
				JR		C, .DB76
				CP		#01
				CALL	C, _BEEP
				JR		C, .DB76
; Atualiza contador.
				LD		B, A
; Sobe cursor na tela.
				POP		AF
				LD		C, A
				CALL	_COUT
; Atualiza ponteiro.
				LD		DE, (DLNG)
				XOR		A
				SBC		HL, DE
; Atualiza POS().
				LD		A, (DLNG)
				LD		C, A
				LD		A, (CURPOS)
				SUB		C
				LD		(CURPOS), A
				JP		.loop

; Retorna se n�o estiver entre os valores v�lidos para um Token
.DBC4:			CP		$80			; Primeiro token v�lido.
				RET		C
				CP		$F0			; �ltimo Token poss�vel + 1
				RET		NC

; Localiza a palavra reservada correspondente.
				SUB		#7F
				LD		C, A
				LD		DE, _MNEMTB
.DBDD:			LD		A, (DE)
				INC		DE
				OR		A
				JP		P, .DBDD
				DEC		C
				JR		NZ, .DBDD
.DBE6:			AND		#7F
; Enquanto n�o enche o buffer...
				LD		C, A
				LD		A, B
				CP		#FF
				LD		A, C
				JR		NC, .DC06
				LD		(HL), C
; Copia cada letra da palavra reservada para o buffer.
				LD		(IPHFLG), A
				INC		HL
				INC		B
				CALL	_PRINTAPOS
				LD		A, (DE)
				INC		DE
				OR		A
				JP		P, .DBE6
				
; Fim normal.
				POP		DE
				POP		AF
				JP		.loop

; Fim por buffer cheio.
.DC06:			POP		DE
				POP		AF
				JP		.printAndNext
				
; Trata DELETE E RUBOUT
.FN_DEL:
				CALL	.FN_BS
				RET		C
.FN_RUB:		DEC		B
				PUSH	AF, DE, BC, HL
		; Move buffer pra esquerda.
				CALL	.getSize
				PUSH	BC				; Preserva BC para reusar no atualizaTela
				LD		DE, HL
				INC		HL
				LDIR		
				
		; Atualizar tela
.atualizaTela:	POP		BC				; Recupera o BC salvo acima
				LD		B, C
				POP		HL				; Recarrega HL
				PUSH	HL
				CALL	MSG.CHARS
				POP		HL, BC, DE, AF
				RET

.FN_INS:		INC		B		
				PUSH	AF, DE, BC, HL
		; TODO mover buffer pra direita
				CALL	.getSize
				PUSH	BC				; Preserva BC para reusar no atualizaTela
				ADD		HL, BC
				LD		DE, HL
				DEC		DE
				LDDR
				POP		HL				; Recarrega HL
				PUSH	HL
				LD		(HL), " "
				LD		A, (.CURRENT_SIZE)
				INC		A
				LD		(.CURRENT_SIZE), A
				JR		.atualizaTela

.getSize:		PUSH	HL				; Preserva HL
				LD		DE, (.BUFFER)
				AND		A				; Clear Carry
				SBC		HL, DE
				LD		BC, HL
				POP		HL
				RET

; Trata Ctrl+H.
.FN_BS:			LD		A, ASCII.BS
; Se B < 2, apenas emite bip e reinicia la�o.
				LD		C, A
				LD		A, B
				CP		#02
				CALL	C, _BEEP
				RET		C
				LD		A, C
; Decrementa contador e ponteiro.
				DEC		B
				DEC		HL	
				CALL	_PRINTAPOS
				JP		NZ, .loop


GETLIN_ORG_SIZE			EQU		$DC0B - $DA9B 	 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
													; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
													; (tem margem pra isso, mas d� mais trabalho!)
GETLIN_TEST_BED_END		EQU		$
GETLIN_TEST_BED_SIZE		EQU		GETLIN_TEST_BED_END - GETLIN_TEST_BED_START
	DISPLAY	"new size ",/H,GETLIN_TEST_BED_SIZE
	DISPLAY	"org size ",/H,GETLIN_ORG_SIZE
	DISPLAY "GETLIN diferen�a ",/D,(GETLIN_ORG_SIZE - GETLIN_TEST_BED_SIZE)
				