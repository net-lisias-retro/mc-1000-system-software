	OUTPUT	bin/GETLIN.BAS
	ORG	$0000

	INCLUDE		"mockups.def"
	INCLUDE		"mc1000.def"

FILEHEADER:
.filename:	DB		"GETLN"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT

			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			CALL	GETL.INIT			; TODO Trocar por JP, e economizar o RET abaixo
			RET

	INCLUDE		mc1000_pvt.def
	INCLUDE		PSGIO.def
	INCLUDE		MC6847.def
	INCLUDE		MC6845.def
	INCLUDE		KEYBRD.def
	INCLUDE		SCREEN.def
	INCLUDE		CONSOLE.def
	INCLUDE		PSGIO.inc
	INCLUDE		MC6847.inc
	INCLUDE		MC6845.inc
	INCLUDE		randomseed.inc
	INCLUDE		cp_hlde.inc
	INCLUDE		FASTMATH.inc
	INCLUDE		"KEYBRD.inc"
	INCLUDE		"SCREEN.inc"
	INCLUDE		CONSOLE.inc
	INCLUDE		GETLIN.inc
	INCLUDE		MSG.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
			