# Firmware::BASIC #

Artigos com alterações, consertos e expansões para o BASIC do MC-1000.

## Conteúdo ##
* [GETLIN](source/GETLIN.asm)
	* Rotina com expansões e adaptação à nova SKEY?
	* Ver [documentação](docs/GETLIN.md) para detalhes.

No diretório [binários](bin) estão os programas montados e executáveis a partir do BASIC do MC-1000:

* _**.BAS**_ para ser copiados num pendrive/SD do BlueDrive
* _**.wav**_ para ser carregado pela porta cassete.
* _**.cas**_ para ser usado em emuladores

## Consumo de ROM ##

Como o objetivo é que estas rotinas migrem para a ROM, monitorar o consumo do espaço em ROM é necessário. No momento estamos com o seguinte score, onde os valores dão a economia (positivo) ou gasto (negativo) em relação à rotina original:

Módulo          | Código | Dados
---------------:|:------:|:------------:
GETLIN          |     39 | --
