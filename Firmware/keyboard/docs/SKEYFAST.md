# Firmware::Teclado::Fast SKEY? #

## Descrição ##
Rotina SKEY? ($C027) alterada para usar a [FASTKEYPRESSED?](./FASTKEY.md) .

### AsmDoc ###
N/A - Esta rotina é uma prova de conceito, não será "promovida" para produção.

### Status ###

Testada e funcionando.

### *Known Issues* ###

* O Cursor está piscando estupidamente mais rápido na nova rotina. :-) 

## Instruções ##

O programa aguarda por uma tecla e imprime o código ASCII da tecla pressionada duas vezes:

* Na 1ª, usando a rotina original na ROM;
* Na 2ª, usando a rotina alterada.

O programa percebe se o operador não soltar a tecla, e tráva (com mensagem) até que este libere o teclado para então solicitar nova ação.

Os códigos ASCII retornados pelas duas chamadas devem, óbviamente, ser iguais ou a rotina alterada apresentou defeito. Neste caso, por favor notifique o problema [Google Plus](https://plus.google.com/u/0/+LisiasToledo/posts/Yd314B7Z6Fj).

## Fonte Comentado ##

Arquivos fontes da solução:

* [Arquivo principal](../source/SKEYFAST.asm)
* Externals
	* [Rotina FASTKEYPRESSEED?](../source/FASTKEYPRESSED.inc)
	* [Rotina GETKEYBLINE](../source/GETKEYBLINE.inc)

### Arquivo principal ###

O programa é trivial. A rotina SKEY? do firmware foi inserida no programa e alterada para chamar a FASTKEYPRESSED? e este é o único ponto interessante deste programa.

Transplantar esta alteração para a ROM do aparelho, embora rendensse frutos, não compensa devido à implementação de uma [nova SKEY?](SKEYNEW.md), com esta e outras melhorias.

### Externals ###

Ver [FASTKEY](FASTKEY.md) e [Matrix Keboard](KEY_MTRX.md) para descrição detalhada de GETKEYBLINE e FASTKEYPRESSEED?.

## Histórico ##

Durante os *brainstormings* sobre um novo firmware para o BlueDrive do MC-1000 (ver [aqui](https://docs.google.com/document/d/1lE8UUJbeeELHQdDuEFQ91SMQBU89wqB1dXI4H9zsLOw/edit?usp=sharing)), detectou-se a possibilidade de [otimizar a leitura do teclado](FASTKEY.md).

Este programa é um *Test Bed* para a FASTKEYPRESSED? para garantir que seu funcionamento não quebre a atual implementação da SKEY?, na eventualidade da nova ROM do MC-1000 implementar esta otimização.

