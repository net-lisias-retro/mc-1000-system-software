# Firmware::Teclado::FASTKEYPRESSED? #

## Descrição ##
Rotina para detecção acelerada de tecla pressionada. Não informa qual, no entando. Deve-se chamar SKEY? para identificar a tecla.

### AsmDoc ###

	; ==========
	; KEYBRD: Módulo driver do teclado.
	
	; ======
	; KEYBRD.KEYPRESSED? : Checa se tem alguma tecla pressionada (sem se preocupar com qual)
	; Destrói A, B e F
	; Retorno:
	;	flag Z:
	;		1 se nenhuma tecla está pressionada (Zero teclas pressionadas)
	; 		0 se uma ou mais tecla(s) está(ão) pressionadas (NãoZero teclas pressionadas)
	;		Teclas de modificação (SHFT e CTRL) não contam.
	; 	A:
	;		o status do CNTRL e SHFT nos bits 7 e 6. Lógica negativa. (não contam como tecla pressionada)

### Status ###

Testada e funcionando.

### *Known Issues* ###

* O Cursor está piscando estupidamente mais rápido na nova rotina. :-) 

* Falta uma forma padronizada de chamar a FASTKEYPRESSED? . Ela é útil por si só, possivelmente vale a pena adicionar um JMP na tabela de entrypoints.

* Como não houve a preocupação de fazer debouncing, existe a chance da SHFT e da CNTRL repicarem, e a FASTKEYPRESSED? pode acabar mandando bits inválidos para as modificadoras.

	* TODO: construir um *Test Bed* específico para detectar o bouncing do teclado. 

## Instruções ##

Nada de especial: carregar, rodar e seguir as instruções da tela.

## Fonte Comentado ##

Arquivos fontes da solução:

* [Arquivo principal](../source/FASTKEY.asm)

### Arquivo principal ###

O programa é trivial, o pulo do gato é a forma como ler a matriz do teclado. Ver próxima seção.

### FASTKEYPRESSEED? ###

Originalmente, a rotina SKEY fazia uma varredura completa da matriz de teclado (para se valer de uma característica do hardware, ver [SKEYNEW](docs/SKEYNEW.md) para detalhes).

A contagem de T-Cycles da rotina `GETPSG0F` (que como explicado no artigo supramencionado, é chamada uma vez para cada tecla do teclado) é

Instrução   | T-Cycles
------------|:--------
DI          | 4
LD A, $F    | 8
OUT ($20),A | 11
IN A,($40)  | 11
EI			| 4
RET			| 10   

Totalizando 48 T-Cycles (mais 18 para o CALL, 65). Sendo necessárias 48 chamadas obrigatórias até se descobrir que não há tecla pressionada, acabamos que temos aí 3120 T-Cycles gastos só aí (e estou ignorando o tempo gasto pela rotina chamadora).

Para agilizar a detecção de uma tecla pressionada, então, pode-se alternativamente selecionar **todas** as linhas para serem lidas de uma vez. Se qualquer coisa estiver pressionada, um ou mais bits estarão setados, e então sabemos que precisamos varrer a matriz.

Se nenhum bit estiver setado, não há nenhuma tecla pressionada e podemos simplesmente pular todo o processamento da matriz, porque já sabemos o resultado. O ganho em performance é substancial.

Perdemos alguns ciclos para o caso menos comum (ter uma tecla pressionada), e salvamos alguns bons milhares para o caso mais comum (não ter nenhuma tecla pressionada). Isso vai acelerar inclusive o BASIC, que fica fazendo checagens constantes se tem ou não uma tecla pressionada.

O retorno do Status das teclas Control e Shift agiliza o processamento da SKEY?, otimizando o "pior caso". Desta forma, evitamos a necessidade da rotina consumidora ter que probar o teclado uma vez à mais para detectar o estado das modificadores. Com este pequeno *insigh* tornamos o pior caso no mínimo tão eficiente quanto a rotina original, o que deixa o caso mais comum ainda mais lucrativo!

### GETPSG0FMASKED ###

Uma nova alteração foi proposta à ROM, uma rotina chamada GETPSG0FMASKED de 8 bytes que por questões de performance e espaço deve ser posicionada imediatamente antes da GETPSG0F no monitor. O ideal é que a GETPSG0F não mude de endereço, já que ela tem potencial para ser usada por programas na user land.

Observe que o Carry deve estar setado para que as interruções sejam restauradas ao final. Esta alteração foi necessária porque a rotina de leitura com debouncing precisa manter as interrupções desligadas para evitar que a interrupção do PSG reprograme as portas do PSG no meio do debouncing.

Como o objetivo da FASTKEYPRESSED? é detectar o mais rápido possível se tem ou não uma tecla pressionada, não nos preocupamos com bouncing.

## Histórico ##

Durante os *brainstormings* sobre um novo firmware para o BlueDrive do MC-1000 (ver [aqui](https://docs.google.com/document/d/1lE8UUJbeeELHQdDuEFQ91SMQBU89wqB1dXI4H9zsLOw/edit?usp=sharing)), eu percebi que o firmware do MC-1000 checava teclado de uma maneira bem parecida com a que o TK-2000 faz : a mais estúpida possível. :-D

O que acontece é que **toda** a rotina de varredura da matriz de teclado é executada, independente de haver ou não uma tecla pressionada. Uma otimização simples e intuitiva (e que acelerou até mesmo os jogos do TK-2000, que passaram a se comportar de forma mais parecida com o Apple II velocidade!) é simplesmente setar _**todas**_ as linhas da matriz do teclado para serem lidas de uma vez - e então com um único IN, você sabe se tem uma tecla pressionada ou não. Você não tem a menor idéia de qual, mas sabe que tem ao menos uma e isto basta por enquanto.

Esta rotina resolve um pouco desta estupidez. :-D

### Discussão ###

O Emerson (ensjo) acha que a rotina deve ignorar as teclas Shift e Control, retornando "no key" mesmo se estas estiverem. Isso deve oferecer alguma otimização para quando a GETKEY? estiver trabalhando, uma vez que estas teclas só são úteis em combinação com alguma coisa (e a varredura da malha de teclado lida com estas duas teclas uma vez que isso acontece).

Por outro lado, eu tenho a percepção de que esta rotina pode ser útil também para jogos que usam o teclado (e neste caso, a shift ou a control dariam excelentes botões de tiro, porque são sempre detectadas não importa a linha da matriz selecionada). Mas talvez uma rotina dedicada para jogos que usem o teclado seja mais adequada no frigir dos ovos.

Sugestões? (no [Google Plus](https://plus.google.com/+LisiasToledo/posts/MoKgPDYEpRb))
