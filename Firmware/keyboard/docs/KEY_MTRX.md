# Firmware::Teclado::Keyboard Matrix #

## Descrição ##

Keyboard Matrix: programas para teste da matriz de teclado. Versão com Debounce e sem.

Um modelo de device driver é implementado.

### AsmDoc ###

	; ==========
	; PSG: Módulo Driver do PSG.
	
	; ======
	; PSG.GETKEYBLINE: Lê em A a linha especificada pela máscara em B com debounce de 7ms.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A: Máscara lógica inversa com as teclas pressionadas na linha selecionada
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.GET0FMASKED: Lê em A a linha especificada pela máscara em B. Se Carry = 0, não reativa as interrupções.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A: Máscara lógica inversa com as teclas pressionadas na linha selecionada
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.GET0F: Lê em A o registrador #0F do PSG. (INPUT). Sempre reativa as interrupções.
	; Idealmente, ficará no mesmo endereço da GETPSG0F original para garantir retrocompatibilidade
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A:
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.GET0F: Lê em A o registrador #0F do PSG. (INPUT). Se Carry = 0, não reativa as interrupções.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A:
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.


### Status ###

Testada e funcionando.

### *Known Issues* ###

* O Debounce de 7ms tá ajustado no chute. Descobrir se dá pra diminuir, ou se precisa aumentar.
* Falta um *Test Bed* específico para detectar o bouncing - o debouncing foi implementado por "inércia", com influência de esperiências com o TK-2000.
* Ainda não sei se é pepino da matrix do teclado, ou se é pau de *software*, mas ao teclar R, J e K ao mesmo tempo, 4 teclas são identificadas. =/

## Instruções ##

Carregar, rodar e observar a matriz de teclado sendo atualizada na tela na medida em que teclas são pressionadas.

Não há limite para a quantidade de teclas pressionadas simultanemante.
 
SHFT+RESET para sair: os programas fica em loop contínuo.

A versão [com bouncing](../source/KEYBMTRX.asm) foi feita para checar empiricamente o custo do [debouncing](../source/KEYDMTRX.asm).

## Fonte Comentado ##

*Work in Progress*

* WIP
* Externals
	* [Rotina GETKEYBLINE](../source/GETKEYBLINE.inc)

### Arquivo principal ###

O programa é trivial, os comentários no fonte devem ser suficientes para o entendimento.

Ao rodar, fica varrendo a matriz de teclado imprimindo o valor dos registradores de linha de teclado em binário para fácil visualização.

### GETKEYBLINE.inc ###

Um problema chato ao ler teclas, sejam elas *push buttons* ou (pior) tecladinho chiclete de controle remoto é o efeito de Boucing.
![](../../../Hardware/Drivers/docs/debouncing.gif)

Uma rotina de debouncing é necessária para evitar que o algoritmo leia a flutuação durante o acionamento/desacionamento da tecla. A rotina que faz a leitura mascarada com debounce é a GETKEYBLINE .

A GETPSG0FMASKED faz a mesma leitura, mas sem debouncing. Ela é, óbviamente, bem mais rápida mas está sujeita ao bouncing das teclas.

### Externals ###

Ver [FASTKEY](FASTKEY.md) para descrição detalhada de `PSG.GET0FMASKED`.

## Histórico ##

Durante o desenvolvimento da nova SKEY?, diversos problemas (indo desde erros de lógica até flutuação do barramento de dados - trimmar o delay para debounce deu algum trabalho) puderam ser solucionados com este *test bed*. 

Nota para mim mesmo: eu devia ter começado com isto, teria identificado o problema do debounce umas 3 horas trás. :-P

