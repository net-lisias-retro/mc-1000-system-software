# Firmware::Teclado::New SKEY? #

## Descrição ##
Nova Rotina SKEY? ($C027) que, planeja-se, substituirá a atual no firmware da BD OS 2.0 .

A nova implementação adiciona *features* (ver AsmDoc), corrige bugs, otimiza a performance geral da máquina e possui uma implementação sensivelmente menor que a original (14 bytes no momento). Mas depende de uma tabela com 192 (ou 144 com alguma reserva) bytes e de rotinas que precisam ser encaixada antes da GETPSG0F em $C465 ,  de forma que a solução é deficitária em gasto de memória ROM.

Um modelo de device driver é implementado.

### AsmDoc ###

Ver [Driver de Teclado](../../../Hardware/Drivers/docs/KEYBRD.md) para detalhes.

### Status ###

Implementados e testados.


### *Known Issues* ###

* No modo game, ocorreu um falso positivo para o CHR$(127) em algumas combinações de teclas.
	* Não foi definido se é um erro obscuro e intermitente no algoritmo, ou se meu MC-1000 tá precisando trocar os capacitores de desaclopamemnto
	* Ou pode ser o debouncing que tá muito pequeno.
* Tambem no modo game, se mais de 8 teclas forem pressionadas quebra a formatação da tela 

## Instruções ##

### SKEYNEW BASIC ###

O programa aguarda por uma tecla e imprime o código ASCII da tecla e o caractere respectivo: Caractere Controle, letra comum ou TOKEN BASIC. Percebe quando o operador não soltar a tecla, e tráva até que este libere o teclado para então solicitar nova ação.

O programa está em loop infinito. SHIFT+RESET para resetar o sistema.

Há duas versões, [SKEYNEWB](../source/SKEYNEWB.asm) e [SKEYNEWG](../source/SKEYNEWG.asm) - para testar a rotina em modo BASIC e modo GAME respectivamente. 

Como teste de conceito, foi implementado um mapa de teclado alternativo, onde a tecla CONTROL gera todos os caracteres de controle ASCII (e alguns códigos especiais como F1 a F0 através de CTRL-1 até CTRL-0), e os tokens do BASIC são gerados pela combinação SHIFT+CTRL.

O mapa abaixo detalha a implementação:

![](../../../Hardware/Drivers/docs/mc1000-teclado-extendido.png)

Notação:

* Branco na metade de baixo : tecla sem modificadores
* Branco na metade de cima : tecla com SHIFT
* Amarelo, Vermelho ou Laranja (embaixo da tecla, alinhado à direita): tecla com CONTROL
* Azul (acima da tecla, centralizado) : tecla com SHIFT **E** CONTROL.

### SKEYNEW GAME ###

O programa imprime a boas vindas, e entra num loop perpétuo (teclar SHFT+RESET para sair):

* Aguarda 1 segundo (para dar tempo do operador digitar uma combinação de teclar)
* Varre a matriz de teclado, reconhecendo até 10 teclas simultâneas
* Imprime as teclas que forem reconhecidas
* recomeça

## Fonte Comentado ##

Arquivos fontes da solução:

* Arquivos principais
	* [Modo BASIC](../source/SKEYNEWB.asm)
	* [Modo GAME](../source/SKEYNEWG.asm)
* Externals
	* Device Driver do Teclado
		* [Documentação](../../../Hardware/Drivers/docs/KEYBRD.md)
		* [def](../../../Hardware/Drivers/source/KEYBRD.def)
		* [inc](../../../Hardware/Drivers/source/KEYBRD.inc)

### Arquivos Principais ###

[SKEYNEWB.asm](../source/SKEYNEWB.asm) e [SKEYNEWG.asm](../source/SKEYNEWG.asm) são os *test beds* para a implementação, respectivamente para o MODO BASIC e o MODO GAME.

O algoritmo de varredura de teclado é menos complexo, mas mais eficiente que o original. Ver Device Driver do Teclado para mais informações.

## Histórico ##

Durante os *brainstormings* sobre um novo firmware para o BlueDrive do MC-1000 (ver [aqui](https://docs.google.com/document/d/1lE8UUJbeeELHQdDuEFQ91SMQBU89wqB1dXI4H9zsLOw/edit?usp=sharing)), ao implementar a [otimização da leitura do teclado](FASTKEY.md), observou-se que a presente implementação em ROM impõe uma série de restrições e efeitos colaterais bizarros que não só atrapalham o uso, mas chegam a impossibilitar a implementação de algumas idéias que foram levantadas.

Uma nova implementação se tornou necessária, do contrário boa parte das (boas) idéias deverá ser abandonada ou no mínimo exigirá algumas gambiarras para serem implementadas.
