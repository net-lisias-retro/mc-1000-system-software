# Firmware::Teclado #

Artigos com consertos, alterações e exansões para o firmware de Teclado do MC-1000.

## Conteúdo ##

* [FASTKEYPRESSED?](source/FASTKEY.asm)
	* rotina para otimizar a detecção de uma tecla pressionada.
	* Ver [documentação](docs/FASTKEY.md) para detalhes.

* [FAST SKEY?](source/SKEYFAST.asm)
	* Rotina SKEY? alterada para usar a FASTKEYPRESSED?
	* Ver [documentação](docs/SKEYFAST.md) para detalhes.

* Keyboard Matrix [Bounced](source/KEYBMTRX.asm) e [Debounced](source/KEYDMTRX.asm)
	* Programas para visualizar (em binário) um mapa da matriz de teclado
	* Ver [documentação](docs/KEY_MTRX.md) para detalhes.

* [NEW SKEY?](source/SKEYNEW.inc) [modo BASIC](source/SKEYNEWB.asm) e [modo GAME](source/SKEYNEWG.asm)
	* Rotina SKEY? reimplementada, programas para testar o modo BASIC e GAME
	* Ver [documentação](docs/SKEYNEW.md) para detalhes.


No diretório [binários](bin) estão os programas montados e executáveis a partir do BASIC do MC-1000:

* _**.BAS**_ para ser copiados num pendrive/SD do BlueDrive
* _**.wav**_ para ser carregado pela porta cassete.
* _**.cas**_ para ser usado em emuladores

## Consumo de ROM ##

Como o objetivo é que estas rotinas migrem para a ROM, monitorar o consumo do espaço em ROM é necessário. No momento estamos com o seguinte score, onde os valores dão a economia (positivo) ou gasto (negativo) em relação à rotina original:

Módulo          | Código | Dados
---------------:|:------:|:------------:
SKEY?           |     48 | -144 ou -192
FASTKEYPRESSED? |    -12 | --

Nota: uma rotina foi movida para o Monitor (RANDOMSEED) para reuso em outros módulos.
