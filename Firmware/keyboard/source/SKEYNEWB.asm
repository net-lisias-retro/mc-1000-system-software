	OUTPUT	bin/SKEYNEWB.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"

FILEHEADER:
.filename:	DB		"KEYNW"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD		A, $FF
			LD		(HEAD), A		; Garante modo BASIC
			LD		HL, PAYLOAD
			LD		DE, PAYLOAD_start
			LD		BC, PAYLOAD_size
			LDIR
			JP		PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			CALL	KEYBRD.INIT
			
	; Mensagem 
			LD		HL, str_mensagem
			CALL	SYS.MSG
			
1:			CALL	KEYBRD.CIN?
			OR		A
			JR		Z, 1b
			; Ok, temos uma tecla em KEYS0
			CALL	PRINTKEY		; Imprimimos o dado
2:			CALL	KEYBRD.KEYPRESSED?
			JR		NZ, 2b			; Esperamos o usu�rio largar a tecla, para n�o sair repetindo
			JR		1b				; E l� vamos n�s de novo.
			

PRINTKEY:			
			LD		HL, str_keypressed1
			CALL	SYS.MSG

			LD		IY, KEYBRD.KEY_BUFFER
			LD		HL, (IY)
			LD		A, (HL)
			LD		H, 0
			LD		L, A
			CALL	PRNTHL

			LD		HL, str_keypressed2
			CALL	SYS.MSG
			
			LD		HL, str_keypressed3
			CALL	SYS.MSG
			LD		HL, (IY)
			LD		A, (HL)
			CALL	PRINT_CHAR

			LD		HL, str_newline
			JP		SYS.MSG
			
PRINT_CHAR:
			CP		$F0
			JP		NC, .FUNCTION
			CP		$80
			JP		NC, .TOKEN
			CP		$60
			JP		NC, .MINUSCULAS
			CP		$20
			JP		NC, .COMUM
			
			; Se chegamos aqui, � caractere de controle.
			
			LD		C, 4		; Cara string tem 4 bytes
								; A j� cont�m o caractere de controle
			CALL	SYS.MPY		; HL <- A * C
			LD		DE, char_control_tbl
			ADD		HL, DE		; HL agora aponta para a string
			JP		SYS.MSG		; Imprime.
			
.COMUM:
			LD		(maiuscula.data), A
			LD		HL, maiuscula.msg
			JP		SYS.MSG		; Imprime.

.MINUSCULAS:
			SUB		$20			; Converte para maiusculas.		
			LD		(minuscula.data), A
			LD		HL, minuscula.msg
			JP		SYS.MSG		; Imprime.

.TOKEN:
			JP		$DCDA		; Imprime palavra reservada em A
			
.FUNCTION:
			SUB		$F0
			LD		C, 4
			CALL	SYS.MPY
			LD		DE, func_control_tbl
			ADD		HL, DE		; HL agora aponta para a string
			JP		SYS.MSG		; Imprime.
			
			
;						-12345678901234567890123456789012
str_mensagem:  		DB	"TESTE DA NEW SKEY? ($C027) REESC"
					DB	"ITA PARA ELIMINAR AS IDIOSSINCRA"
					DB	"SIAS DA IMPLEMENTACAO DA ROM E  "
					DB	"PERMITIR CUSTOMIZACOES.\R\N"
					DB	"MODO BASIC."
str_newline:		DB	"\R\N",ASCII.NUL
str_keypressed1:	DB	"TECLA: ",ASCII.NUL
str_keypressed2:	DB	"  ",ASCII.NUL
str_keypressed3:	DB	"ASCII: ",ASCII.NUL

minuscula:
.msg:				DB	"MIN("
.data:				DB	00
					DB	")",ASCII.NUL
					
maiuscula:
.msg:				DB	00, ASCII.NUL
.data				EQU	.msg

func_control_tbl:
					DB	"F10",ASCII.NUL
					DB	"F1 ",ASCII.NUL
					DB	"F2 ",ASCII.NUL
					DB	"F3 ",ASCII.NUL
					DB	"F4 ",ASCII.NUL
					DB	"F5 ",ASCII.NUL
					DB	"F6 ",ASCII.NUL
					DB	"F7 ",ASCII.NUL
					DB	"F8 ",ASCII.NUL
					DB	"F9 ",ASCII.NUL
					DB	"PAU",ASCII.NUL
					DB	"BRK",ASCII.NUL
					DB	"CAP",ASCII.NUL
					DB	"RUB",ASCII.NUL
					DB	"INS",ASCII.NUL
					DB	"UNK",ASCII.NUL

char_control_tbl:
					DB	"NUL",ASCII.NUL
					DB	"SOH",ASCII.NUL
					DB	"STX",ASCII.NUL
					DB	"ETX",ASCII.NUL
					DB	"EOT",ASCII.NUL
					DB	"ENQ",ASCII.NUL
					DB	"ACK",ASCII.NUL
					DB	"BEL",ASCII.NUL
					DB	"BS ",ASCII.NUL
					DB	"HT ",ASCII.NUL
					DB	"LF ",ASCII.NUL
					DB	"VT ",ASCII.NUL
					DB	"FF ",ASCII.NUL
					DB	"CR ",ASCII.NUL
					DB	"SO ",ASCII.NUL
					DB	"SI ",ASCII.NUL
					DB	"DLE",ASCII.NUL
					DB	"DC1",ASCII.NUL
					DB	"DC2",ASCII.NUL
					DB	"DC3",ASCII.NUL
					DB	"DC4",ASCII.NUL
					DB	"NAK",ASCII.NUL
					DB	"SYN",ASCII.NUL
					DB	"ETB",ASCII.NUL
					DB	"CAN",ASCII.NUL
					DB	"EM ",ASCII.NUL
					DB	"SUB",ASCII.NUL
					DB	"ESC",ASCII.NUL
					DB	"FS ",ASCII.NUL
					DB	"GS ",ASCII.NUL
					DB	"RS ",ASCII.NUL
					DB	"US ",ASCII.NUL

	INCLUDE		mc1000_pvt.def
	INCLUDE		PSGIO.def
	INCLUDE 	KEYBRD.def
	INCLUDE 	KEYBRD.inc
	INCLUDE		randomseed.inc
	INCLUDE		PSGIO.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
