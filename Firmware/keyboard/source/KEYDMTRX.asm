	OUTPUT	bin/KEYDMTRX.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"

	
FILEHEADER:
.filename:	DB		"KEYDM"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD		A, $FF
			LD		(HEAD), A		; Garante modo BASIC
			LD		HL, PAYLOAD
			LD		DE, PAYLOAD_start
			LD		BC, PAYLOAD_size
			LDIR
			JP		PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			
.loop:			
	; Mensagem 
			LD		HL, str_mensagem
			CALL	SYS.MSG
			
			LD		HL, str_cabecalho
			CALL	SYS.MSG

			LD		B, %11111110			; Linha 0
			LD		C, "0"
			CALL	PRINT_LINE
			
			LD		B, %11111101			; Linha 1
			LD		C, "1"
			CALL	PRINT_LINE
			
			LD		B, %11111011			; Linha 2
			LD		C, "2"
			CALL	PRINT_LINE
			
			LD		B, %11110111			; Linha 3
			LD		C, "3"
			CALL	PRINT_LINE
			
			LD		B, %11101111			; Linha 4
			LD		C, "4"
			CALL	PRINT_LINE
			
			LD		B, %11011111			; Linha 5
			LD		C, "5"
			CALL	PRINT_LINE
			
			LD		B, %10111111			; Linha 6
			LD		C, "6"
			CALL	PRINT_LINE
			
			LD		B, %01111111			; Linha 7
			LD		C, "7"
			CALL	PRINT_LINE
			
			JP		.loop 
	
PRINT_LINE:
			PUSH 	BC
			CALL	SYS.COUT
			LD		C, ":"
			CALL 	SYS.COUT
			
			POP		BC
			SCF		; Para reativar as interrup��es.
			CALL	PSGIO.GETKEYBLINE		
			CALL	PRINT_BIN
			
			LD		HL, str_newline
			JP		SYS.MSG
			
PRINT_BIN:
			LD		C, 8
.loop:		RRA
			PUSH	AF
			PUSH	BC
			LD		HL, str_1
			JR		C, 1f
			LD		HL, str_0
1:			CALL	SYS.MSG
			POP		BC
			POP		AF
			DEC		C
			JR		NZ, .loop
			RET
			
;						-12345678901234567890123456789012
str_mensagem:  		DB	$1E	; Inicio de tela
					DB	"     MAPA MATRIZ DE TECLADO     "
str_newline:		DB	"\R\N",ASCII.NUL
str_cabecalho: 		DB	"   0  1  2  3  4  5  6  7\R\N",ASCII.NUL
str_0				DB	" 0 ",ASCII.NUL
str_1				DB	" 1 ",ASCII.NUL

	INCLUDE		mc1000_pvt.def
	INCLUDE		PSGIO.def
	INCLUDE		PSGIO.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
