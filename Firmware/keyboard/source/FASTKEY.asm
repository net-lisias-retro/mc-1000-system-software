	OUTPUT	bin/FASTKEY.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"	
	
FILEHEADER:
.filename:	DB		"FTKEY"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			
	; Mensagem 
			LD		HL, str_mensagem
			CALL	SYS.MSG
			
		; N�o chamamos INIT desta vez, a KEYPRESSED? n�o precisa e n�o quero desestabilizar a m�quina sem necessidade.
			
1:			CALL	KEYBRD.KEYPRESSED?
			JR		Z, 1b
			
			PUSH	AF
			
			LD		HL, str_keypressed
			CALL	SYS.MSG

			POP		AF
			RLCA
			JR		C, 1f
			LD		HL, str_withcontrol
			PUSH	AF
			CALL	SYS.MSG
			POP		AF
1:
			RLCA
			JR		C, 1f
			LD		HL, str_withshift
			CALL	SYS.MSG
1:
			LD		HL, str_newline
			JP		SYS.MSG			

;						-12345678901234567890123456789012
str_mensagem:  		DB	"TESTE DA ROTINA QUE VAI ACELE- "
					DB	"RAR A DETECCAO DE TECLA PRESSIO"
					DB  "NADA DA ATUAL IMPLEMENTACAO DA "
					DB	"KEY? ($C009).\R\N"
					DB	"<WAITING FOR A KEY>"
str_newline:		DB	"\R\N",ASCII.NUL
str_keypressed		DB	"TECLA PRESSIONADA ",ASCII.NUL
str_withcontrol		DB	"COM CONTROL ",ASCII.NUL
str_withshift		DB	"COM SHIFT ",ASCII.NUL


	INCLUDE		mc1000_pvt.def
	INCLUDE		PSGIO.def
	INCLUDE		KEYBRD.def
	INCLUDE		PSGIO.inc
	INCLUDE		randomseed.inc
	INCLUDE		KEYBRD.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
