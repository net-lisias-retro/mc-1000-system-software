FASTKEYPRESSED?_TEST_BED_START		EQU		$

; ==========
; KEYBRD: M�dulo driver do teclado.

	MODULE	KEYBRD

; ======
; KEYBRD.KEYPRESSED? : Checa se tem alguma tecla pressionada (sem se preocupar com qual)
; Destr�i A, B e F
; Retorno:
;	flag Z:
;		1 se nenhuma tecla est� pressionada (Zero teclas pressionadas)
; 		0 se uma ou mais tecla(s) est�(�o) pressionadas (N�oZero teclas pressionadas)
;		Teclas de modifica��o (SHFT e CTRL) n�o contam.
; 	A:
;		o status do CNTRL e SHFT nos bits 7 e 6. L�gica negativa. (n�o contam como tecla pressionada)
;
KEYPRESSED?:
					LD		B, 0
					SCF
					CALL	PSGIO.GET0FMASKED
					LD		B, A		; Preserva o A
					OR		%11000000	; Ignora modificadores
					INC		A			; Seta Z flag
					LD		A, B		; Restaura A (para retornar os modificadores).
					RET

	ENDMODULE
	
FASTKEYPRESSED?_ORG_SIZE			EQU		0 		 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
														; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
														; (tem margem pra isso, mas d� mais trabalho!)
FASTKEYPRESSED?_TEST_BED_END		EQU		$
FASTKEYPRESSED?_TEST_BED_SIZE		EQU		FASTKEYPRESSED?_TEST_BED_END - FASTKEYPRESSED?_TEST_BED_START
	DISPLAY	"new size ",/H,FASTKEYPRESSED?_TEST_BED_SIZE
	DISPLAY	"org size ",/H,FASTKEYPRESSED?_ORG_SIZE
	DISPLAY "FASTKEYPRESSED? diferen�a ",/D,(FASTKEYPRESSED?_ORG_SIZE - FASTKEYPRESSED?_TEST_BED_SIZE)


; *********
; Source code Compatibility

FASTKEYPRESSED?		EQU		KEYBRD.KEYPRESSED?

