	OUTPUT	bin/SKEYFAST.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"	
	
FILEHEADER:
.filename:	DB		"KEYFT"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			
	; Mensagem 
			LD		HL, str_mensagem
			CALL	SYS.MSG
			
			LD		HL, str_oldskey
			CALL	SYS.MSG			
1:			CALL	SYS.KEY?
			JR		Z, 1b
			CALL	PRINTKEY
			
			CALL	FASTKEYPRESSED?
			JR		Z, 2f
			LD		HL, str_soltetecla
			CALL	SYS.MSG 
1:			CALL	FASTKEYPRESSED?
			JR		NZ, 1b
			
2:						
			LD		HL, str_newskey
			CALL	SYS.MSG			
1:			CALL	SKEY?
			JR		Z, 1b
			CALL	PRINTKEY
			
			RET

PRINTKEY:
			LD		A, (KEY0)
			PUSH	AF
			
			LD		HL, str_keypressed
			CALL	SYS.MSG

			POP		AF
			LD		H, 0
			LD		L, A
			CALL	PRNTHL

			LD		HL, str_newline
			JP		SYS.MSG
			
;						-12345678901234567890123456789012
str_mensagem:  		DB	"TESTE DA SKEY? ($C027) USANDO A"
					DB	"ROTINA FASTKEYPRESSED? AO INVES"
					DB	"DE VARRER BURRAMENTE A MATRIZ  "
					DB	"DO TECLADO."
str_newline:		DB	"\R\N",ASCII.NUL
str_oldskey:		DB	"<USING OLD SKEY?>", ASCII.NUL
str_newskey:		DB	"<USING NEW SKEY?>",ASCII.NUL
str_keypressed:		DB	" TECLA : ",ASCII.NUL
str_soltetecla:		DB	"SOLTE A TECLA!\R\N",ASCII.NUL

; ======
; SKEY?: Procura status do teclado para jogo.
; Retorna #00 se nenhuma tecla pressionada. #FF se tecla pressionada, e o c�digo da tecla
; fica armazenado em KEY0 (#011B).
; (Esse � o comportamento em modo "BASIC" [HEAD = #FF].
; Em "modo de jogo" [HEAD = 0], retorna a quantidade de teclas simult�neamente pressionadas [at� 4],
; com as teclas armazenadas nos endere�os de KEY0 a KEY0+3.)
SKEY?:			; C37F
	PUSH    HL
	PUSH    DE
	PUSH    BC

; Chama hooks.
	CALL	JOB
	CALL	JOBM

; Atualiza n�mero aleat�rio.
	CALL	RANDOMSEED	; Aqui uma rotina de 10 bytes foi trocada por um call de 3.
						; Temos um super�vit de 7 bytes
	CALL	FASTKEYPRESSED?	; Estes 6 bytes foram compensados movendo a rotina de
	JP		Z, .K15			; random seed acima.
; Aqui temos agora um super�vit de 1 byte

; B vai variar de #FE (11111110) a #7F (01111111) para selecionar cada "linha"
; do teclado.
; Em modo de jogo (HEAD [$0106] = 0), C vai contar a quantidade de teclas
; simult�neas (0~3).
				LD		BC,#FE00

; Seleciona linha do teclado indicada por B.
.K1:			DI      ; Impede que a interrup��o peri�dica do Z80 (0x0038) que
                        ; por padr�o � configurada para chamar a rotina de som (INTRUP)
                        ; se intrometa e altere a sele��o dos registradores do PSG.
				LD		A,#0E
				OUT		(REG), A
				LD		A,B
				OUT		(WR), A
				CALL	GETPSG0F

; D vai variar de 0 a 5 para selecionar cada bit da linha do teclado.
				LD		D,#00
.K3:			RRCA
				LD		E,A
				CALL	NC, .K4 ; tecla pressionada
; "next D"
				INC		D		; Incrementa coluna
				LD		A,D
				CP		#06
				LD		A,E
				JP		C, .K3
; "next B"
				LD		A,B
				RLCA
				LD		B,A
				JP		C, .K1

.K2: ; Nenhuma tecla pressionada?
				LD		A,C
				OR		A
				JP		Z, .K15

; Alguma tecla pressionada.
; Retorna #FF em modo BASIC.
				LD		A,(HEAD)
				INC		A
				LD		A,#FF
				JP		Z, .KEY3

; Retorna quantidade de teclas (1~4) em modo de jogo.
				LD		A, (KEY0)
				CALL	NEXTGM_
				LD		A,C
				RRCA
				RLCA
				JP		.KEY3

.K4:			; CTRL pressionado?
				CALL	GETPSG0F
				RLCA
				JP		NC, .K9	; testa CTRL

; CTRL n�o pressionado.
				LD		A,D
				CP		#04
				JP		NC, .K5

; L = D * 8 + 64
.K0:			ADD		A,#06
; L = D * 8 + 16
.K5:			ADD		A,#02
; L = D * 8
.K10:			ADD		A,A
				ADD		A,A
				ADD		A,A
				LD		L,A

; SHIFT pressionado?
				CALL	GETPSG0F
				AND		#40
				JP		NZ, .K11

; SHIFT pressionado.
				LD		A,D
				CP		#04
				LD		A, #F0
				JP		NC, .K12
				LD		A,#20
.K12:			ADD		A,L
				LD		L,A
; H = posi��o do bit apagado em B.
.K11:			LD		H,#00
				PUSH	BC
.K6:			LD		A,B
				RRCA
				LD		B,A
				JP		NC, .K7
				INC		H
				JP		.K6
.K7:			POP		BC
				LD		A,H
				ADD		A,L
				PUSH	AF
; Em modo BASIC, vai para os ajustes finais.
				LD		A,(HEAD)
				INC		A
				JP		Z, .K70		; em BASIC
; Em modo de jogo, armazena a tecla pressionada em KEY0+C
				LD		A,C
				LD		HL, KEY0
				ADD		A,L
				LD		L,A			; KEY0 fora da tela
				POP		AF
				LD		(HL),A
; Incrementa C. Se C < 4, continua a varredura do teclado.
				INC		C
				LD		A,C
				CP		#04
				RET		C

; Sen�o, termina a varredura.
				POP		AF		; despreza a pilha
				JP		.K2

; Ajustes finais.
; [ --> RETURN
; \ --> SPACE
; ] --> RUBOUT
; < --> ,
; = --> -
; > --> .
; ? --> /
; , --> <
; - --> =
; . --> >
; / --> ?
.K70:			POP		AF
				POP		DE		; Despreza a pilha para CNC K4
				LD		H,#00	; e ajusta valores para 2C-3C
				LD		L,A
				AND		#FC
; Corrige < = > ? (+16).
				CP		','
				JP		NZ, .K13
				LD		H,#10 ; +16
; Corrige , - . / (-16).
.K13:			CP		'<'
				JP		NZ, .K14
				LD		H,#F0 ; -16
.K14:			LD		A,L
				ADD		A,H
; Corrige ENTER.
				CP		'['
				LD		B,#0D ; tecla RETURN
				JP		Z, .K20
; Corrige SPACE.
				CP		'\\'
				LD		B,' ' ; tecla SPACE
				JP		Z, .K20
; Corrige RUBOUT.
				CP		']'
				JP		NZ, .K21
				LD		B,#7F ; tecla RUBOUT
; Armazena c�digo da tecla pressionada.
.K20:			LD		A,B
.K21:			LD		(KEY0), A
; A = #FF indica que houve tecla pressionada.
				LD		A,#FF
				JP		.KEY3
; CTRL pressionado
.K9:
				LD		A,D
				CP		#04
				JP		C, .K10
				JP		.K0
; Nenhuma tecla pressionada: c�digo de retorno = 0.
.K15:			LD		A,#00
;
.KEY3:			POP		BC
				POP		DE
				POP		HL
				RET
C464:
	NOP		; Gastando o super�vit para n�o mudar os entrypoints das rotinas abaixo!

; ------- ------ --------
; Resto da ROM do aparelho
; ------- ------ --------

; blabalbalbalablablabalab

; ------- ------ --------
; Hole in the ROM in 0xF563 0xFFFF
; ------- ------ --------

	INCLUDE		mc1000_pvt.def
	INCLUDE		PSGIO.def
	INCLUDE		PSGIO.inc
	INCLUDE		FASTKEYPRESSED.inc
	
; ======
; Atualiza n�mero aleat�rio.
;
; Destr�i HL, DE, F
RANDOMSEED:
	LD      HL,(RANDOM)
	LD      DE,#B2E7
	ADD     HL,DE
	LD      (RANDOM),HL
	RET


PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
