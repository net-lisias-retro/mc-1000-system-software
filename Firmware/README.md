# Firmware #

Artigos para consertos e substituições para o Firmare do MC-1000.

## Conteúdo ##

Expansões e substituições para as rotinas:

* [do Conector de Jogos](joystick/README.md)
* [de Teclado](keyboard/README.md)
* [do Monitor](monitor/README.md)
* [do BASIC](BASIC/README.md)

## Consumo de ROM ##

Como o objetivo é que estas rotinas migrem para a ROM, monitorar o consumo do espaço em ROM é necessário. No momento estamos com o seguinte score, onde os valores dão a economia (positivo) ou gasto (negativo) em relação à rotina original:

Módulo          | Código | Dados
---------------:|:------:|:------------:
keyboard        |    -8  | -144 ou -192
Monitor         |    24  |
BASIC           |    39  |  

