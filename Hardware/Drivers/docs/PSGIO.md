# Hardware::PSG::I/O

## Descrição

Keyboard Matrix: programas para teste da matriz de teclado. Versão com Debounce e sem.

Um modelo de device driver é implementado.

### AsmDoc

	; ==========
	; PSG.IO: Módulo Driver de I/O do PSG.
	
	; ======
	; PSG.IO.GETKEYBLINE: Lê em A a linha especificada pela máscara em B com debounce de 7ms.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A: Máscara lógica inversa com as teclas pressionadas na linha selecionada
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.IO.GET0FMASKED: Lê em A a linha especificada pela máscara em B. Se Carry = 0, não reativa as interrupções.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A: Máscara lógica inversa com as teclas pressionadas na linha selecionada
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.IO.GET0F: Lê em A o registrador #0F do PSG. (INPUT). Sempre reativa as interrupções.
	; Idealmente, ficará no mesmo endereço da GETPSG0F original para garantir retrocompatibilidade
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A:
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.
	
	; ======
	; PSG.IO.GET0F: Lê em A o registrador #0F do PSG. (INPUT). Se Carry = 0, não reativa as interrupções.
	; Destrói:
	;		AF, IFF
	; Retorno:
	;		A:
	;			Os bits são normalmente ligados.
	;			Pressionar uma tecla zera o bit correspondente. O bit 6 é sempre SHIFT e o bit 7 é sempre CTRL.


### Status ###

Testada e funcionando.

### *Known Issues* ###

* O Debounce de 7ms tá ajustado no chute. Descobrir se dá pra diminuir, ou se precisa aumentar.
* Falta um *Test Bed* específico para detectar o bouncing - o debouncing foi implementado por "inércia", com influência de esperiências com o TK-2000.
* Ainda não sei se é pepino da matrix do teclado, ou se é pau de *software*, mas ao teclar R, J e K ao mesmo tempo, 4 teclas são identificadas. =/

## Fonte Comentado ##

*Work in Progress*

* WIP

### GETKEYBLINE ###

Um problema chato ao ler teclas, sejam elas *push buttons* ou (pior) tecladinho chiclete de controle remoto é o efeito de Boucing.
![](debouncing.gif)

Uma rotina de debouncing é necessária para evitar que o algoritmo leia a flutuação durante o acionamento/desacionamento da tecla. A rotina que faz a leitura mascarada com debounce é a GETKEYBLINE .

A GETPSG0FMASKED faz a mesma leitura, mas sem debouncing. Ela é, óbviamente, bem mais rápida mas está sujeita ao bouncing das teclas.

## Histórico ##

Durante o desenvolvimento da nova SKEY?, diversos problemas (indo desde erros de lógica até flutuação do barramento de dados - trimmar o delay para debounce deu algum trabalho) puderam ser solucionados com este *test bed*. 

Nota para mim mesmo: eu devia ter começado com isto, teria identificado o problema do debounce umas 3 horas trás. :-P

