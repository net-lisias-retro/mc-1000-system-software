# Hardware :: Device Drivers :: IRQ Handlers #

## MIRQ ##

O Z80 responde por MIRQs (Masked IRQ) **apenas** entre instruções. Ele **sempre** termina a instrução corrente (independente dela ter prefixos ou não - a máquina de estado do Z80 lida bem com isso) antes de checar se o pino /INT tá ativo, e somente neste caso ele aceita e processa a MIRQ.

De forma que o circuito interruptor precisa manter a /INT ativa até ser servido, do contrário a interrupção se perderá.

Isto é especialmente importante para o MC-1000 uma vez que a interrupção periódica criada pelo NE555 não se preocupa em garantir serviço: ele lança o pulso, se o Z80 pescar, pescou - se não, dane-se. Com apenas um circuito solicitando MIRQ, isto não é um problema - mas se alguém pendurar mais alguma coisa no barramento do MC-1000, algumas precauções precisam ser tomadas.

Também vale mencionar que o Z80 só volta à reconhecer MIRQs *uma instrução* após o `EI`, e não imediatamente após. Como normalmente as rotinas de tratamento terminam com a dupla `EI ; RETI` , isto significa que somente após o `RETI` haverá a possibilidade de outra MIRQ ser servida. Isto é interessante porque previne loops acidenteais do handler antes de limpar o *stack*, prevenindo *overflows*.

Esta última informação deixa implícita a possibilidade da própria MIRQ *handler* ser interrompível - o que, efetivamente, pode ser útil quando mais de um dispositivo está aguardando serviço.


## NMI ##

No presente momento, as NMIs são solemente ignoradas pelo *firmware* do MC-1000. Se uma ocorrer, é *crash* de sistema.

Contudo, possivelmente a NMI é uma melhor opção para uam IRQ do Blue Drive que a MIRQ, de forma que será implementado um handler NMI configurável.
