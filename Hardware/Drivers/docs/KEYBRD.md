# Hardware::Driver::Teclado

## Descri��o
Device Driver de Teclado que, planeja-se, substituir� a atual no firmware da BD OS 2.0 .

A nova implementa��o adiciona *features* (ver AsmDoc), corrige bugs, otimiza a performance geral da m�quina e possui uma implementa��o sensivelmente menor que a original (14 bytes no momento). Mas depende de uma tabela com 192 (ou 144 com alguma reserva) bytes e de rotinas que precisam ser encaixada antes da GETPSG0F em $C465 ,  de forma que a solu��o � deficit�ria em gasto de mem�ria ROM.

### AsmDoc ###

	; ===========
	; KEYBRD: M�dulo Driver do Teclado
	KEY_COUNT			; Quantidade de teclas simult�neas identific�veis. 1 byte
	KEY_BUFFER		; Endere�o do Buffer para armazenar as teclas. 2 bytes
	TABLE				; Endere�o da Tabela com 192 bytes com o mapa de teclado. 2 bytes
	
	; ======
	; KEYBRD.INIT: Inicializa e Reseta Device Driver KEYBRD (Leitura de teclado)
	; Normalmente chamada apenas durante a inicializa��o do aparelho.
	; Destr�i: A, HL
	INIT:
	
	; ======
	; KEYBRD.RESET: Reseta Device Driver SKEY? (Leitura de teclado)
	; Restaura o status do teclado para os padr�es default.
	; Destr�i: A, HL
	
	; ======
	; KEYBRD.KEYPRESSED? : Checa se tem alguma tecla pressionada (sem se preocupar com qual)
	; Destr�i A, B e F
	; Retorno:
	;	flag Z:
	;		1 se nenhuma tecla est� pressionada (Zero teclas pressionadas)
	; 		0 se uma ou mais tecla(s) est�(�o) pressionadas (N�oZero teclas pressionadas)
	;		Teclas de modifica��o (SHFT e CTRL) n�o contam.
	; 	A:
	;		o status do CNTRL e SHFT nos bits 7 e 6. L�gica negativa. (n�o contam como tecla pressionada)
	;

	; ======
	; KEYBRD.CIN?: Retorna status do teclado.
	; Destr�i: IX, IY, AF
	; Configura��o:
	;		HEAD ($0106):
	;			Modo de opera��o: $FF para BASIC, $00 para GAME. Ver Retorno.
	;			Em modo BASIC, faz debounce da leitura do PSG. Em modo game n�o.
	;		SKEY?.KEY_COUNT (endere�o ainda por definir):
	;			Em modo GAME, configura quantas teclas simult�neas a SKEY? vai reconhecer. Default 4.
	;		SKEY?.KEY_BUFFER (endere�o ainda por definir):
	;			Configura o endere�o onde as teclas reconhecidas ser�o armazenadas. Default KEY0 (#011B)
	;			Deve apontar para um buffer de tamanho condizente � GAME_KEYCOUNT ou haver� buffer overrun.
	;		SKEY?.TABLE  (endere�o ainda por definir):
	;			Configura o endere�o para o mapa de matriz de teclado corrente. Default setado pelo firmware.
	; Retorno:
	;	A:
	;		$00 se nenhuma tecla pressionada. 
	;		EXASC.UNK ($FF) se tecla pressionada, e o c�digo da tecla fica armazenado em KEYBOARD_BUFFER,
	;			se modo "BASIC" [HEAD = $FF].
	;		Quantidade de teclas simult�neamente pressionadas [configur�vel, ver GAME_KEYCOUNT], com as teclas armazenadas no buffer apontado por KEYBOARD_BUFFER.
	;			se "modo de jogo" [HEAD = 0].
	;		IY apontando para o KEYBOARD BUFFER corrente.
	;	F(Z):
	;		Se Z, Zero Teclas Pressionadas
	;		Se NZ, N�o Zero (uma ou mais) Teclas Pressionadass 


### Status ###

Implementados e testados.


### *Known Issues* ###

* No modo game, ocorreu um falso positivo para o CHR$(127) em algumas combina��es de teclas.
	* N�o foi definido se � um erro obscuro e intermitente no algoritmo, ou se meu MC-1000 t� precisando trocar os capacitores de desaclopamemnto
	* Ou pode ser o debouncing que t� muito pequeno.

## Fonte Comentado ##

Arquivos fontes do device driver:

* [KEYBRD.def](../source/KEYBRD.def) 
* [KEYBRD.inc](../source/KEYBRD.inc) 
* Externals
	* PSG IO
		* [def](../source/PSGIO.def)
		* [inc](../source/PSGIO.inc)

#### O Problema ####

A solu��o original fazia a convers�o das coordenadas linha x coluna em ASCII code se valendo de uma caracter�stica f�sica da matriz de teclado do MC-1000 - dada uma coluna (bit 0, por exemplo), variando as 8 linhas da 0 at� a 7 voc� obtinha uma sucess�o de c�digos ASCII. Ent�o era basicamente saber qual o c�digo ASCII de cada primeira linha/coluna e sair contando at� achar uma tecla ativada. Mas, �bvio, h� exce��es (como as teclas de controle, o ENTER, o RUBOUT, etc) que ent�o precisam ser tratadas caso � caso.

![](KeyboardMatrix.png)

Possivelmente por falta de espa�o em ROM no momento da implementa��o, alguns atalhos matutos foram tomados e boa parte das idiossincrasias no comportamento do teclado vem das situa��es n�o tratadas ou de efeitos colaterais imprevistos.

> Apesar de existir quase 2Kb de espa�o ocioso na ROM no MC-1000 Stock, as v�rias camadas de firmware foram implementadas em �pocas diferentes, por pessoas diferentes, para micros diferentes com espa�o dispon�vel diferentes em ROM! Um interessante trabalho de reaproveitamento de componentes de software, mas com evidentes restri��es de tempo e recursos para testes de integra��o.

Em cima disso, a seq��ncia de instru��es para mudar a linha a ser lida na matriz de teclado � relativamente extensa, e esta abordagem de parse de teclado exige 6 colunas vezes 8 linhas igual a _**48**_ chamadas � rotina abaixo:

	; GETPSG0F: L� em A o registrador #0F do PSG.
	; Isto obt�m uma "linha" do teclado.
	; Os bits s�o normalmente ligados.
	C465  F3        DI
	C466  3E0F      LD      A,#0F
	C468  D320      OUT     (#20),A ; REG
	C46A  DB40      IN      A,(#40) ; RD
	C46C  FB        EI
	C46D  C9        RET

As instru��es OUT tamb�m s�o particularmente lerdas no Z-80, e o ideal � que algoritmos de I/O minimizem os acessos sempre que poss�vel.

Por fim, a SKEY? original introduz um atraso de processamento substancial � m�quina como um todo: al�m do algoritmo de parse de matriz de teclado ser extenso e demorado, ele � chamado regularmente pelo BASIC (que precisa monitorar, entre outros, o CTRL-C para dar *BREAK* no programa!). O tempo gasto regularmente para varrer toda a matriz e voltar de m�os vazias n�o � pouco. Esta parte do problema foi solucionada pela `KEYBRD.KEYPRESSED?`.


#### A Solu��o ####

A solu��o proposta e atualmente implementada se baseia no princ�pio dos Algoritmos Simples/Estrutura de Dados complexas. Ao inv�s calcular algoritmicamente o c�digo ASCII em fun��o do hardware do teclado (e depois marretar as exce��es), a nova SKEY? pega a coordenada linha,coluna e pesca o c�digo ASCII numa tabela de refer�ncia. Estas s�o em quatro, uma para cada combina��o de teclas modificadoras:

* Com CONTROL e com SHIFT
* S� com CONTROL
* S� com SHIFT
* Sem nenhuma delas.

Como o algoritmo n�o se baseia mais na posi��o das teclas no hardware, a varredura p�de ser otimizada. Apenas 8 acessos s�o necess�rios para ler o teclado inteiro (um acesso para cada linha), ao inv�s dos 48 da rotina original!

Possuindo a matriz de teclado do MC-1000 8 linhas de 6 colunas cada, cada tabela possui ent�o 8 x 6 = 48 entradas - ou seja, cada tabela ocupa 48 bytes. Sendo necess�rio 4 delas, o custo em mem�ria s� para as tabelas � de 48 x 4 = 192 bytes. O atual firmware n�o faz uso da combina��o CTRL+SHFT, de modo que pode-se simplesmente usar os 48 bytes do final da tabela para outra coisa e deixar quieto (o TK-2000 faz uma parecida, por sinal, e ningu�m morreu por causa disso at� hoje), derrubando a demanda para 144 bytes. Fica ent�o � crit�rio do desenvolvedor criar uma tabela completa se for o caso.

O algoritmo tamb�m foi quebrado em duas vers�es especializadas, uma para o modo BASIC e outra para o modo GAME (com reuso do algoritmo de parsing, �bvio). Isto permitiu eliminar um ou dois jumps condicionais dentro do loop principal. Mas o ganho de performance foi neglig�vel (at� porque o loop principal � de 8 itera��es, contra os 48 anteriores), o objetivo foi deixar a coisa mais simples de implementar e manutenciar depois. Um b�nus � a possibilidade de se scanear mais de 4 teclas simult�neas em modo GAME, uma vez que a restri��o agora n�o est� mais *hardcoded* no parser, mas � um crit�rio da rotina principal que pode ser configur�vel (ver `GAME_KEYCOUNT` e `KEYBOARD_BUFFER`).

Um ponteiro na �rea de sistema (`KEYBOARD_TABLE`) foi reservado para apontar para a tabela, de forma que � poss�vel criar vers�es customizadas desta tabela  - algo �til para quando uma 80 colunas for implementada (pois ela dar� suporte para caracteres min�sculos, que na implementa��o original era imposs�vel gerar).

A forma padronizada de configura��o de mapa de teclado abre possibilidades interessantes para jogos e aplicativos implementarem seus pr�prios mapas especializados de teclado sem nenhum esfor�o de programa��o. Customiza��o de teclas de controle e fun��o para jogos tornou-se trivial. Por exemplo, um piano digital pode ser facilmente implementado reconhecendo at� 10 teclas simult�neas. Mesmo 20 (piano � quatro m�os) � teoricamente poss�vel no novo algoritmo.

Contudo, algumas combina��es de teclas (RJK confirmado) d�o falsos positivos para teclas adicionais. Talvez falta de corrente na linha (e, talvez, um problema do meu aparelho)? Testes em outros aparelhos s�o necess�rios.

### Externals ###

Ver [PSG I/O](../source/PSGIO.def) para descri��o detalhada de GETKEYBLINE.

## Hist�rico ##

Durante os *brainstormings* sobre um novo firmware para o BlueDrive do MC-1000 (ver [aqui](https://docs.google.com/document/d/1lE8UUJbeeELHQdDuEFQ91SMQBU89wqB1dXI4H9zsLOw/edit?usp=sharing)), ao implementar a [otimiza��o da leitura do teclado](FASTKEY.md), observou-se que a presente implementa��o em ROM imp�e uma s�rie de restri��es e efeitos colaterais bizarros que n�o s� atrapalham o uso, mas chegam a impossibilitar a implementa��o de algumas id�ias que foram levantadas.

Uma nova implementa��o se tornou necess�ria, do contr�rio boa parte das (boas) id�ias dever� ser abandonada ou no m�nimo exigir� algumas gambiarras para serem implementadas.
