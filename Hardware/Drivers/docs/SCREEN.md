# Hardware :: Device Drivers :: Screen #

## Terminal

O modelo de terminal adotado pelo firmware foi o [ADM-3A](https://en.wikipedia.org/wiki/ADM-3A), e a especificação de software foi mantida (embora o mapeamento no teclado tenha sido alterado ou extendido).

Novos comandos ** foram implementados usando a especificação do [VT-220](https://en.wikipedia.org/wiki/VT220).

O comando ESC= do ADM-3A não foi implementado corretamente no *firmware* do MC-1000. No momento, optou-se por "nivelar por baixo" e abraçar o erro como norma - é possível uma implementação *full-complaiant* com o ADM-3A ao mesmo tempo que se mantêm o *status quo* mas o retorno deste esforço é incerto (afinal, código gasta bytes na ROM).

ASCII	|	Hex	|	BASIC		|		No teclado			|	Descrição
:------:|:-----:|:-------------:|:-------------------------:|:----------
**BEL**		| $07	| `CHR$(7)`		| <CTRL>+<G>				| Emite um bip.
**BS**		| $08	| `CHR$(8)`		| <CTRL>+<H>				| O cursor anda uma coluna para a esquerda.
**LF**		| $0A	| `CHR$(10)`	| <CTRL>+<J>				| O cursor desce uma linha.
**VT**		| $0B	| `CHR$(11)`	| <CTRL>+<K>				| O cursor sobe uma linha.
**FF**		| $0C	| `CHR$(12)`	| <CTRL>+<L>				| O cursor anda uma coluna para a direita.
**CR**		| $0D	| `CHR$(13)`	| <CTRL>+<M> ou <RETURN>	| O cursor vai para a primeira coluna da linha.
**SO**		| $0E	| `CHR$(14)`	| <CTRL>+<N>				| Texto reverso. *
**SI**		| $0F	| `CHR$(15)`	| <CTRL>+<O>				| Texto normal. *
**SUB**		| $1A	| `CHR$(26)`	| <CTRL>+<Z>				| Limpa a tela (e o cursor continua onde está).
**ESC**		| $1B	| `CHR$(27)`	| <CTRL>+<=>				| Início de sequência de escape para posicionamento de cursor. *
**RS**		| $1E	| `CHR$(30)`	| <CTRL>+<,>				| O cursor vai para o início da tela. *
**DEL**		| $7E	| `CHR$(127)`	| <RUBOUT>					| O cursor anda uma coluna para a esquerda, apagando caracteres. *

Seqüência ESC

ASCII         | BASIC                           |  Descrição
:------------:|:-------------------------------:|:------------
**ESC**=*yx*  | `CHR$(27)+"="+CHR$(y)+CHR$(x)`  |  Move o cursor para a posição (y,x) (onde y e x são índices de 0 até o máximo do modo de tela atual). Se posição não existe no modo de vídeo atual, o comando é solenemente ignorado.
**ESC**[K     | `CHR$(27)+"[K"`		            |  Limpa do cursor até o fim da linha **
**ESC**[J     | `CHR$(27)+"[J"`		            |  Limpa do cursor até o fim da tela **
**ESC**[L     | `CHR$(27)+"[L"`		            |  Insere uma linha onde está o cursor, rolando conteúdo para baixo **
**ESC**[M     | `CHR$(27)+"[M"`		            |  Apaga uma linha onde está o cursor, rolando conteúdo para cima **
**ESC**D      | `CHR$(27)+"D"`                  |  Rola a tela inteira uma linha para baixo. **
**ESC**M      | `CHR$(27)+"M"`                  |  Rola a tela inteira uma linha para cima. **
**ESC**#0     | `CHR$(27)+"#0"`                 |  Desliga cursor. ***
**ESC**#1     | `CHR$(27)+"#1"`                 |  Religa o cursor. ***
**ESC**#6     | `CHR$(27)+"#6"`                 |  Modo 32 colunas. **
**ESC**#5     | `CHR$(27)+"#5"`                 |  Modo 80 colunas se disponível. **

\* Especificação difere em relação ao firmware original.

** Adendo à especificação original (tanto do firmware como do ADM-3A)

*** Invenção pura e simples.

Nota: se um comando não pode ser atendido, ele é integralmente e silenciosamente ignorado.

## Discussão

### Padrão de ESCape sequences

Num primeiro momento, considerou-se adotar o padrão [Tektronix 4014](https://en.wikipedia.org/wiki/Tektronix_4010) uma vez que os caracteres de controle são muito semelhantes ao do ADM-3A.

Contudo, algumas funcionalidades implementadas não estão presentes no 4014, e inventar comandos é algo que planeja-se evitar. Um padrão ruim é melhor que nenhum padrão, e esta foi a filosofia adotada nesta nova emulação de terminal do MC-1000.

Buscar inspiração na [VIDEX do Apple 2](https://archive.org/details/Videx_Videoterm_Installation_and_Operation_Manual) revelou que esta seguiu o padrão [VT52](https://en.wikipedia.org/wiki/VT52) - que não só não possui todas as funcionalidades desejadas como ainda difere do próprio [VT100](https://en.wikipedia.org/wiki/VT100), quanto mais do VT200.

Na ausência de um padrão usável semelhante ao ADM-3A, optou-se por preencher as lacunas usando comandos do VT200 - substituir todos os comandos do ADM-3A e transformar o MC-1000 num terminal ANSI/VT200 até foi cogitado, mas prontamente descartado. Não faz sentido alijar todo o *software* legado da plataforma por questões meramente estéticas.

De novo, um padrão ruim é melhor que nenhum padrão. :-)

Como curiosidade, investigando a documentação do [xterm](http://invisible-island.net/xterm/) observou-se dilema semelhante.

### Presença do cursor

O cursor pode ser tratado de duas formas: constantemente ligado, ou ligado apenas durante INPUT de teclado.

Discussão no [Facebook](https://www.facebook.com/groups/mc1000/permalink/1174606422590875/) e [Google Plus](https://plus.google.com/u/0/+LisiasToledo/posts/fprwPwWkPfw).

### Offset do ESC =

No ADM-3A, a seqüência ESC=<y><x> posiciona o cursor para a coordenada X,Y na tela. O MC-1000 seguiu mal e porcamente o padrão, pois o ADM-3A usa um offset de 32 nas duas coordenadas (ou seja, vc emite ESC=<(32+y)><(32+x)> mas no MC-1000 o Y está sem offset e o X é reconhecido para valores menores que 32. Valores maiores que 32 são subtraídos de 32, de forma que 0 e 32 são a mesma coordenada X.

Logo, `ESC=\b\b` manda o cursor para 0,0 - assim como `ESC=\0\0` o faz no MC-1000. =/

Como a implementação do MC-1000 já quebrou o padrão ADM-3A, e como a placa de 80 colunas nunca saiu (de forma que não há software escrito para este hardware), optou-se por simplificar a implementação. O offset não foi implementado nem no Y nem no X.

A racionalização é que além da óbvia complicação de ter duas formas de endereçar coluna, a linha não segue esta regra. E isto quebra completamente a compatibilidade com o ADM-3A e já que a compatibilidade foi pro saco, eu optei por capar fora o offset de 32 na coluna - e agora a coluna é endereçada a partir 0 exatamente como a linha.

Isto também facilita detectar erros - esta confusão acabou deixando o firmware legado com a detecção de erros comprometida, e com as poucas otimizações de tela desativadas.

A pegadinha é que se alguém usou o endereçamento de coluna com offset de 32, o posicionamento vai falhar (porque o firmeware percebe e não deixa passar).
