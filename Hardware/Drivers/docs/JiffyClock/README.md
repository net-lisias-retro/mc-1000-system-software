# Firmware::Monitor::JiffyClock

## Descrição

O MC-1000 possui um NE555 configurado como [oscilador estável](http://www.electronics-tutorials.ws/waveforms/555_oscillator.html) para gerar interrupções regulares. O /MIRQ é usado para virar a rotina de música do PSG.

![](NE555.png)

O cálculo da freqüência pode ser feito pelo seguinte programa:

	10 rem 555 Oscillator Frequency Equation
	20 c = .01 * 1.00E-6 : rem 103 ou .01Z (Z = +20% a -80% de tolerância) - 10nF no schematic
	30 r1 = 390000
	40 r2 = 1000
	100 rem calculation
	110 t1 = 0.693*(r1 + r2)*c
	120 t2 = 0.693 * r2 * c
	130 t = t1 + t2
	140 dt = (r1+r2)/(r1+2*r2)
	150 f = 1/t
	200 rem results
	210 print "t1,t2,T (ms):",t1,t2,t
	220 print "Duty Cycle (%):",dt*100
	230 print "F (Hz):", f
	
	
	> run
	t1,t2,T (ms):	2.709630E-03 	6.930000E-06 	2.716560E-03 
	Dut	y Cycle (%):	99.744898 
	F (Hz):	368.112613 
 

Com base neste circuito, deu-se a idéia de implementar um [Jiffy Clock](https://en.wikipedia.org/wiki/Jiffy_%28time%29), nos moldes do usado pelo Commodore C64. Contudo, ao passo que o do C64 NTSC pulsa a 59.94 Hz (16.8 ms), o nosso vai pulsar a ~368.11 Hz (~2.71 ms).

Numa tentativa de minimizar os efeitos da variação do oscilador (mas não que seja possível tornar a solução precisa), os ticks são contados 1 a 1. Isso significa que para contar a passagem de 24 horas teremos:

	24 * 60 * 60 * 368.112613 = 31804929.7632
	~31804929
	$1E54E01
	
Essa abordagem usa 25 bits, usaremos então 4 bytes para a contagem e voltaremos à zero (meia noite) no 31804929º tick.

Com isso, um clock usável pode ser implementado, e mesmo usado para alimentar o timestamp de arquivos (uma vez que uma solução para a data seja implementada num file system).

Tasks regulares também podem ser implementados, e talvez um ON TIMER(n) GOSUB possa ser implementado no BASIC.


### AsmDoc

	*TODO*

### Status

* Work in progress *

### *Known Issues*

*TODO*

## Instruções

Nada de especial: carregar, rodar e seguir as instruções da tela.

## Fonte Comentado

Arquivos fontes da solução:

*TODO*

### Arquivo principal

*TODO*

### Medições in Situ

Até o momento, foram feitas medições na saída do 555 (pino 3) em quatro máquinas:

* Exemplar A
	* Meu (bem surrado)
	* R16 (1K) e R17 (290K) com tolerância 5% (Gold)
	* C30 cerâmico .01Z (10 nF, de +80 a -20%)
	* de +/- 370.37037 a 289.855072 Hz (5.4 a 6.9 DIV a .5ms por DIV)
		* ![](Medicao_5ms_Exemplar_A.jpg)
		* [Vídeo](Medicao_5ms_Exemplar_A.mp4)
* Exemplar B
	* Também meu (bem conservado)
	* R16 (1K) e R17 (290K) com tolerância 5% (Gold)
	* C30 cerâmico 103 (sem código de tolerância)
	* +/- 357.142857 Hz (5.6 DIV a .5ms por DIV)
		* ![](Medicao_5ms_Exemplar_B.jpg)
* Exemplar C
	* Alexandre Souza 
* Exemplar D
	* Emerson Branco (por Fábio Belavenuto)
	* TBD
	* 290 Hz medido no pino 3 do 555

Como podemos observar, usar um Capacitor de baixa qualidade gera bastante variações no oscilador.
	
### Discussão

Testes empíricos e medições por osciloscópio levaram à uma medição de até 290Hz nesta interrupção. Isso se deve às tolerâncias do capacitor cerâmico usado (de +80% a –20%) em algumas unidades de MC-1000.

(Os resistores são de 5% a 10%, de forma que eles não causam um desvio muito significativo no circuito).

A 290Hz, temos uma diferença de 368-290 = 78Hz - uma diferença de quase 20% para menos da especificação!

Trocar o capacitor de cerâmico para polipropileno (poliéster) com tolerância de 1% ou melhor vai deixar esta interrupção muito mais precisa.

Usando o exemplo acima, temos o seguinte desvio do exemplar usado para a especificação:


| Hz    | Contagem para 1 dia  | Desvio    | Diferença em segundos no fim do dia |
|:------|:---------------------|:---------:|:---|
|368    | 31804929             | --        | -- |
|290    | 25056000             | 6748929   | 18339.4809 |


O que dá quase 5 horas de defasagem no fim do dia entre o exemplar usado nas medições e a especificação!
