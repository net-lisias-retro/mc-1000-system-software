	MODULE MC6847

INIT:
; Inicializa tela de 32 colunas.
IREG:
				LD		A, #01							; Modo alfanumrico verde.
				LD		(MODBUF), A
				OUT		(MC6847.IO.MODE), A
				RET

RESET:
			; Inicializa vari�veis para 32 colunas de caracteres por linha da tela de texto.
				LD		HL, MC6847.DEEP
				LD		(SCRN.VIDEODEF.DEEP), HL

				LD		HL, MC6847.LLNG
				LD		(SCRN.VIDEODEF.LLNG), HL

				LD		HL, MC6847.SNAM_SZ - MC6847.LLNG
				LD 		(SCRN.VIDEODEF.ZNAM), HL

				LD		HL, MC6847.SNAM				; Endere�o de in�cio da tela de texto.
				LD		(SCRN.VIDEODEF.SNAM), HL

				LD		HL, MC6847.ENAM
				LD		(SCRN.VIDEODEF.ENAM), HL

				CALL	BANKV
CLR:			; Limpa tela 32 colunas.
				LD		BC, MC6847.SNAM_SZ-1
				CALL	SCRN.CLRMEM
				JP		SCRN.BANK0

BANKV:			; Habilita VRAM 32 colunas.
				LD		A, (MODBUF)
				AND		#FE
				OUT		(COL32), A
				RET

; Apaga cursor se estiver aceso. (Em 32 colunas.)
; (Necess�rio, por exemplo, antes de mov�-lo.)
UPDBC:			LD		HL, (SCRN.CURSOR.SNPTR)
				LD		A, (HL)
				AND		#80
				JR		NZ, UPDB
				RET

UPDB:			; Exibe/esconde cursor em 32 colunas.
				; (Inverte o caracter na posi��o do cursor.)
				LD		HL, (SCRN.CURSOR.SNPTR)
				LD		A, (HL)
				XOR		#80
				LD		(HL), A
				RET

	ENDMODULE
