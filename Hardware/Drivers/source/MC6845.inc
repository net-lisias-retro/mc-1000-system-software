	MODULE MC6845

PROBE:	; Testa presen�a de monitor de 80 colunas.
		; Retorna Z = 1 se achar
				LD		A, #0F
				OUT		(MC6845.IO.RPORT1), A
				OUT		(MC6845.IO.DPORT1), A
				IN		A, (MC6845.IO.DPORT1)
				CP		$F
				RET

INIT:
; Inicializa registadores.
IREG:			LD		HL, DREG1
				LD		B ,#10
IREG0:			DEC		B
				LD		A, B
				OUT		(MC6845.IO.RPORT1), A
				LD		A, (HL)
				OUT		(MC6845.IO.DPORT1), A
				INC		HL
				JR		NZ, IREG0
				RET

RESET:	; Inicializa vari�veis para 80 colunas.
				LD		HL, MC6845.DEEP
				LD		(SCRN.VIDEODEF.DEEP), HL

				LD		HL, MC6845.LLNG
				LD		(SCRN.VIDEODEF.LLNG), HL

				LD		HL, MC6845.SNAM_SZ - MC6845.LLNG
				LD 		(SCRN.VIDEODEF.ZNAM), HL

				LD		HL, MC6845.SNAM		; Endere�o de in�cio da VRAM.
				LD		(SCRN.VIDEODEF.SNAM), HL

				LD		HL, MC6845.ENAM		; Endere�o do fim da tela de texto da VRAM + 1.
				LD		(SCRN.VIDEODEF.ENAM), HL

				CALL	BANKV
CLR:			; Limpa tela 80 colunas.
				LD		BC, MC6845.SNAM_SZ-1
				CALL	SCRN.CLRMEM
				JP		SCRN.BANK0

BANKV:			; Habilita VRAM 80 colunas.
				LD		A, #01
				OUT		(MC6845.IO.COL80), A
				RET

UPDBC:		; TODO: Esconder o cursor

UPDB:		; Exibe o cursor em 80 colunas.
				LD		HL, (SCRN.CURSOR.SNPTR)
				LD		A, #0E
				OUT		(MC6845.IO.RPORT1), A
				LD		A, H
				SUB		#20
				OUT		(MC6845.IO.DPORT1), A
				LD		A, #0F
				OUT		(MC6845.IO.RPORT1), A
				LD		A, L
				OUT		(MC6845.IO.DPORT1), A
				RET

DREG1:	; Dados para inicializa��o dos registradores do monitor de 80 colunas (MC6845).
				DB		#00		; R15: Cursor (L) <-- 0
				DB		#00		; R14: Cursor (H) <-- 0
				DB		#00		; R13: Start Address (L) <-- 0
				DB		#00		; R12: Start Address (H) <-- 0
				DB		#0B		; R11: Cursor End <-- scan line 11
				DB		#4B		; R10: Cursor Start <-- scan line 11 + bit 5 = piscante
				DB		#0B		; R9: Max Scan Line Address <-- scan line 11
				DB		#A0		; R8: Interlace Mode <-- 160
				DB		#19		; R7: Vertical Sync Position <-- 25 char rows
				DB		#18		; R6: Vertical Displayed <-- 24 char rows
				DB		#02		; R5: Vertical Total Adjust <-- scan line 2
				DB		#1A		; R4: Vertical Total <-- 26 char rows
				DB		#6D		; R3: Horizontal Sync Width <-- 109 chars
				DB		#5C		; R2: Horizontal Sync Position <-- 92 chars
				DB		#50		; R1: Horizontal Displayed <-- 80 chars
				DB		#6D		; R0: Horizontal Total <-- 109 chars

	ENDMODULE
