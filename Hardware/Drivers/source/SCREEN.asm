	OUTPUT	bin/SCREEN.BAS
	ORG	$0000

	INCLUDE		"mc1000.def"

FILEHEADER:
.filename:	DB		"SCRN "
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BINTARGET	EQU		$2000

			TEXTAREA		BASTEXT
PROGRAM:
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004

			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.

			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.

LOADER:
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER

PAYLOAD:
	ENDT

			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SCRN.INIT

	; Mensagem
1:			LD		HL, str_mensagem
			CALL	MY_PMSG
			JR 		1b

	; Checa se temos 80 colunas.
			LD		HL, SCRN.COSW
			BIT		SCRN.COSW.COL80?, (HL)
			JR		Z, 1b		; Se n�o, nada a fazer.

	; Temos 80 colunas.

	; Vamos alternar entre um e outro
			LD		A, (SCRN.COSW.C40?80)
			AND		SCRN.COSW.C40?80.MSK
			JR		NZ, 2f

		; Estamos em 32 colunas. Vamos para 80
			LD		HL, str_80col
			JR		3f

2:		; Estamos em 80 colunas. Vamos para 32
			LD		HL, str_32col
3:			CALL	MY_MSG
			JR		1b


;						-12345678901234567890123456789012
str_mensagem:		DW	.end - $
.data				DB	ASCII.SUB, ASCII.RS, ASCII.BEL	; Limpa a tela, move cursor pro topo, d� beep.
					DB  ASCII.LF, ASCII.LF
					DB	"TESTE ", ASCII.LF, "DO DRIVER", ASCII.VT, "DE SCREEN", ASCII.LF, "QUE"
					DB	ASCII.CR, ASCII.LF, ASCII.LF
					DB	"           SER ALTERADAS DEVIDO", ASCII.CR
					DB	"PRECISARAM", ASCII.LF
					DB	"AS REFATORACOES DO FIRMWARE DO MC-1000 .", ASCII.CR, ASCII.LF

					DB	"TESTE DE ", ASCII.SO, "INVERSE IN", ASCII.SI, " E OUT."

					DB	ASCII.SUB						; Limpa a tela sem mover o cursor.
					DB	"<!>"
					DB	ASCII.CR
					DB	"E AGORA CURSOR!!!!!!!", ASCII.DEL, ASCII.DEL, ASCII.DEL, ASCII.DEL, ASCII.DEL, ASCII.DEL, ASCII.DEL, " !"

					DB	"_"
					DB  ASCII.BS
					DB	ASCII.BS,ASCII.BS,ASCII.VT,ASCII.VT
					DB	ASCII.FF,ASCII.FF,ASCII.FF,ASCII.FF
					DB	ASCII.LF,ASCII.LF,ASCII.LF,ASCII.LF
					DB	ASCII.BS,ASCII.BS,ASCII.BS,ASCII.BS
					DB  ASCII.VT,ASCII.VT,ASCII.FF,ASCII.FF
					DB	"^"

					DB	"_"
					DB  ASCII.BS
					DB  ASCII.BS,ASCII.BS,ASCII.VT,ASCII.VT
					DB	ASCII.FF,ASCII.FF,ASCII.FF,ASCII.FF
					DB	ASCII.LF,ASCII.LF,ASCII.LF,ASCII.LF
					DB	ASCII.BS,ASCII.BS,ASCII.BS,ASCII.BS
					DB  ASCII.VT,ASCII.VT,ASCII.FF,ASCII.FF
					DB	"^"

					DB	"_"
					DB  ASCII.BS
					DB  ASCII.BS,ASCII.BS,ASCII.VT,ASCII.VT
					DB	ASCII.FF,ASCII.FF,ASCII.FF,ASCII.FF
					DB	ASCII.LF,ASCII.LF,ASCII.LF,ASCII.LF
					DB	ASCII.BS,ASCII.BS,ASCII.BS,ASCII.BS
					DB  ASCII.VT,ASCII.VT,ASCII.FF,ASCII.FF
					DB	"^"

					DB	ASCII.ESC,"=",8,16,"8,16"
					DB	ASCII.ESC,"=",4,8,"4,8"
					DB	ASCII.ESC,"=",12,26,"12,26"
					DB	ASCII.ESC,"=",0,0,"0,0"
					DB	ASCII.ESC,"=",1,30,"1,30"
					DB	ASCII.ESC,"=",15,0,"15,0"
					DB	ASCII.ESC,"=",15,29,"15,29"

					DB	ASCII.ESC,"=",1,1
					DB	"*"
					DB  ASCII.BS
					DB  ASCII.BS,ASCII.BS,ASCII.VT,ASCII.VT
					DB	ASCII.FF,ASCII.FF,ASCII.FF,ASCII.FF
					DB	ASCII.LF,ASCII.LF,ASCII.LF,ASCII.LF
					DB	ASCII.BS,ASCII.BS,ASCII.BS,ASCII.BS
					DB  ASCII.VT,ASCII.VT,ASCII.FF,ASCII.FF
					DB	"+"

					DB	ASCII.ESC,"=",15,31
					DB	"*"
					DB  ASCII.BS
					DB  ASCII.BS,ASCII.BS,ASCII.VT,ASCII.VT
					DB	ASCII.FF,ASCII.FF,ASCII.FF,ASCII.FF
					DB	ASCII.LF,ASCII.LF,ASCII.LF,ASCII.LF
					DB	ASCII.BS,ASCII.BS,ASCII.BS,ASCII.BS
					DB  ASCII.VT,ASCII.VT,ASCII.FF,ASCII.FF
					DB	"+"

					DB	ASCII.RS
					DB 	"123456789012345678901234567890"
					DB 	"123456789012345678901234567890"
					DB	ASCII.RS
					DB	"CLR-EOL", ASCII.ESC, "[K"
					DB	"CLR-EOS", ASCII.ESC, "[J"

					DB	ASCII.CR, ASCII.LF, ASCII.LF, ASCII.LF, " TESTE DE SCROLL DE TELA", ASCII.CR
					DB	"01", ASCII.CR, ASCII.LF
					DB	"02", ASCII.CR, ASCII.LF
					DB	"03", ASCII.CR, ASCII.LF
					DB	"04", ASCII.CR, ASCII.LF
					DB	"05", ASCII.CR, ASCII.LF
					DB	"06", ASCII.CR, ASCII.LF
					DB	"07", ASCII.CR, ASCII.LF
					DB	"08", ASCII.CR, ASCII.LF
					DB	"09", ASCII.CR, ASCII.LF
					DB	"10", ASCII.CR, ASCII.LF
					DB	"11", ASCII.CR, ASCII.LF
					DB	"12", ASCII.CR, ASCII.LF
					DB	"13", ASCII.CR, ASCII.LF
					DB	"14", ASCII.CR, ASCII.LF
					DB	"15", ASCII.CR, ASCII.LF
					DB	"16", ASCII.CR, ASCII.LF
					DB	"1234567890123456789012345678901234567890"
					DB	ASCII.LF,ASCII.LF,"----"

					DB	ASCII.ESC,"=",5,1
					DB  ASCII.CR
					DB  "ROLLDOWN"
					DB	ASCII.ESC,"D",ASCII.ESC,"D",ASCII.ESC,"D"
					DB  "ROLLUP"
					DB	ASCII.ESC,"M",ASCII.ESC,"M",ASCII.ESC,"M"
					DB  "ROLLUP"
					DB	ASCII.ESC,"M",ASCII.ESC,"M",ASCII.ESC,"M"
					DB  "ROLLDOWN"
					DB	ASCII.ESC,"D",ASCII.ESC,"D",ASCII.ESC,"D"

					DB	ASCII.SUB, ASCII.RS, ASCII.BEL	; Limpa a tela, move cursor pro topo, d� beep.
					DB	ASCII.ESC,"=",0,0, "12345678901234567890123456789012"
					DB	ASCII.ESC,"=",01,00,"*+",ASCII.ESC,"=",01,30,"!@"
					DB	ASCII.ESC,"=",02,00,"*+",ASCII.ESC,"=",02,30,"!@"
					DB	ASCII.ESC,"=",03,00,"*+",ASCII.ESC,"=",03,30,"!@"
					DB	ASCII.ESC,"=",04,00,"*+",ASCII.ESC,"=",04,30,"!@"
					DB	ASCII.ESC,"=",05,00,"*+",ASCII.ESC,"=",05,30,"!@"
					DB	ASCII.ESC,"=",06,00,"*+",ASCII.ESC,"=",06,30,"!@"
					DB	ASCII.ESC,"=",07,00,"*+",ASCII.ESC,"=",07,30,"!@"
					DB	ASCII.ESC,"=",08,00,"*+",ASCII.ESC,"=",08,30,"!@"
					DB	ASCII.ESC,"=",09,00,"*+",ASCII.ESC,"=",09,30,"!@"
					DB	ASCII.ESC,"=",10,00,"*+",ASCII.ESC,"=",10,30,"!@"
					DB	ASCII.ESC,"=",11,00,"*+",ASCII.ESC,"=",11,30,"!@"
					DB	ASCII.ESC,"=",12,00,"*+",ASCII.ESC,"=",12,30,"!@"
					DB	ASCII.ESC,"=",13,00,"*+",ASCII.ESC,"=",13,30,"!@"
					DB	ASCII.ESC,"=",14,00,"*+",ASCII.ESC,"=",14,30,"!@"
					DB	ASCII.ESC,"=",15,00, "1234567890123456789012345678901"

					DB	ASCII.ESC,"=",08,05," DELETE LINE"

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

					DB	ASCII.ESC,"[M"

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05," LINE DELETED!"
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

					DB	ASCII.ESC,"=",08,05," INSERT LINE    "

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

					DB	ASCII.ESC,"[L"

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05," LINE INSERTED!"
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

					DB	ASCII.ESC,"#0"
					DB	ASCII.ESC,"=",08,05," CURSOR OFF",ASCII.ESC,"[K"

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

					DB	ASCII.ESC,"#1"
					DB	ASCII.ESC,"=",08,05," CURSOR ON "

					; PRA GASTAR TEMPO E ME DEIXAR PENSAR UM POUCO.
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, "!"
					DB	ASCII.ESC,"=",08,05, "/"
					DB	ASCII.ESC,"=",08,05, "-"
					DB	ASCII.ESC,"=",08,05, "\\"
					DB	ASCII.ESC,"=",08,05, " "

.end				EQU	$

str_newline:		DB	ASCII.CR,ASCII.LF,ASCII.NUL
str_32col:			DB	ASCII.ESC,"6",ASCII.NUL
str_80col:			DB	ASCII.ESC,"5",ASCII.NUL

MY_MSG:
1:				LD		A, (HL)
				OR		A
				RET		Z
				LD		C, A
				CALL	SCRN.COUT
				INC		HL
				LD		BC, 125			; Delay de 125 ms
				CALL	_DELAYB
				JR		1b

MY_PMSG:
				LD		A, (HL)
				LD		C, A
				INC		HL
				LD		A, (HL)
				LD		B, A
				INC		HL

1:				LD		A, (HL)
				INC		HL
				PUSH 	BC
				LD		C, A
				CALL	SCRN.COUT
				LD		BC, 125			; Delay de 125 ms
				CALL	_DELAYB
				POP		BC
				DEC		BC
				LD		A, B
				OR		C
				RET		Z
				JR		1b

	INCLUDE		mc1000_pvt.def
	INCLUDE		MC6847.def
	INCLUDE		MC6845.def
	INCLUDE		SCREEN.def
	INCLUDE		SCREEN.inc
	INCLUDE		MC6847.inc
	INCLUDE		MC6845.inc
	INCLUDE		cp_hlde.inc
	INCLUDE		FASTMATH.inc

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
