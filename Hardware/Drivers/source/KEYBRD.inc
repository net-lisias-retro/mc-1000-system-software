KEYBRD_TEST_BED_START		EQU	$

; ===========
; KEYBRD: M�dulo Driver do Teclado

	MODULE KEYBRD
 
; ======
; KEYBRD.INIT: Inicializa e Reseta Device Driver KEYBRD (Leitura de teclado)
; Normalmente chamada apenas durante a inicializa��o do aparelho.
; Destr�i: A, HL
INIT:

; ======
; KEYBRD.RESET: Reseta Device Driver KEYBRD (Leitura de teclado)
; Restaura o status do teclado para os padr�es default.
; Destr�i: A, HL
RESET:				LD		A, 4
					LD 		(KEY_COUNT), A
					LD		HL, KEY0
					LD		(KEY_BUFFER), HL
					LD		HL, KEYBOARD_SCAN_TBL
					LD		(TABLE), HL
					RET

; ======
; KEYBRD.KEYPRESSED? : Checa se tem alguma tecla pressionada (sem se preocupar com qual)
; Destr�i A, B e F
; Retorno:
;	flag Z:
;		1 se nenhuma tecla est� pressionada (Zero teclas pressionadas)
; 		0 se uma ou mais tecla(s) est�(�o) pressionadas (N�oZero teclas pressionadas)
;		Teclas de modifica��o (SHFT e CTRL) n�o contam.
; 	A:
;		o status do CNTRL e SHFT nos bits 7 e 6. L�gica negativa. (n�o contam como tecla pressionada)
;
KEYPRESSED?:
					LD		B, 0
					SCF
					CALL	PSGIO.GET0FMASKED
					LD		B, A		; Preserva o A
					OR		%11000000	; Ignora modificadores
					INC		A			; Seta Z flag
					LD		A, B		; Restaura A (para retornar os modificadores).
					RET

; ======
; KEYBRD.CIN?: Retorna status do teclado.
; Destr�i: IX, IY, AF
; Configura��o:
;		HEAD ($0106):
;			Modo de opera��o: $FF para BASIC, $00 para GAME. Ver Retorno.
;			Em modo BASIC, faz debounce da leitura do PSGIO. Em modo game n�o.
;		SKEY?.KEY_COUNT (endere�o ainda por definir):
;			Em modo GAME, configura quantas teclas simult�neas a SKEY? vai reconhecer. Default 4.
;		SKEY?.KEY_BUFFER (endere�o ainda por definir):
;			Configura o endere�o onde as teclas reconhecidas ser�o armazenadas. Default KEY0 (#011B)
;			Deve apontar para um buffer de tamanho condizente � GAME_KEYCOUNT ou haver� buffer overrun.
;		SKEY?.TABLE  (endere�o ainda por definir):
;			Configura o endere�o para o mapa de matriz de teclado corrente. Default setado pelo firmware.
; Retorno:
;	A:
;		$00 se nenhuma tecla pressionada. 
;		EXASC.UNK ($FF) se tecla pressionada, e o c�digo da tecla fica armazenado em KEYBOARD_BUFFER,
;			se modo "BASIC" [HEAD = $FF].
;		Quantidade de teclas simult�neamente pressionadas [configur�vel, ver GAME_KEYCOUNT], com as teclas armazenadas no buffer apontado por KEYBOARD_BUFFER.
;			se "modo de jogo" [HEAD = 0].
;		IY apontando para o KEYBOARD BUFFER corrente.
;	F(Z):
;		Se Z, Zero Teclas Pressionadas
;		Se NZ, N�o Zero (uma ou mais) Teclas Pressionadass 
CIN?:
					PUSH    HL, DE, BC

; Chama hooks.
					CALL	JOB
					CALL	JOBM
					CALL	@RANDOMSEED			; Atualiza n�mero aleat�rio.
					
					CALL	KEYPRESSED?
					JR		Z, .NOKEYPRESSED

			; Detecta SHFT, CTRL e SHFT+CTRL usando o valor de A retornado por FASTKEYPRESSED?
					AND		%11000000

					LD		DE, KEYBOARD_SCAN_TBL.both
					CP		%00000000
					JR		Z, 1f
					
					LD		DE, KEYBOARD_SCAN_TBL.control
					CP		%01000000
					JR		Z, 1f
					
					LD		DE, KEYBOARD_SCAN_TBL.shift
					CP		%10000000
					JR		Z, 1f

					LD		DE, KEYBOARD_SCAN_TBL.nomod
					
1:					LD		IX, (TABLE)
					ADD		IX, DE						; Seleciona a subtabela adequada.
					
					LD		IY, (KEY_BUFFER)			; Carrega o local do buffer de teclado.

					; Agora IX aponta para o come�o da tabela referente aos modificadores do teclado ativados,
					; e IY aponta para o buffer que receber� as teclas encontradas (normalmente KEY0)

					; Decide se vamos para o modo BASIC ou se modo GAME:
					LD		A, (HEAD)
					INC		A
					JR		Z, .MODO_BASIC
					
.MODO_GAME:
					CALL	.SCAN_START	; Pega a primeira tecla (se chegamos aqui, � porque tem!)
1:					PUSH	AF			; Temos que preservar o A!
					PUSH	BC			; Preserva BC tamb�m
					
					LD		A, L		; Retorno foi em L.
					LD		B, 0		; BC agora � a vers�o 16 bits da qte de teclas lidas em C.
					LD		HL, IY		; Carrega HL com o endere�o do buffer de teclas.
					ADD		HL, BC		; Joga o HL para o offset adequado.
					LD		(HL), A		; Armazena a Czima tecla
					
					POP		BC			; Restaura o B.
					INC		C			; Armazenamos uma tecla, logo incrementamos o contador.

		; Se C < (GAME_KEYCOUNT - normalmente 4), continua a varredura do teclado; se n�o estouramos o buffer.
					LD		A, C
					CP		(KEY_COUNT)
					JR		NC, .exitpopaf
					
					POP		AF			; Restaura A, pois ele tem o �ltimo valor "parseado" da linha corrente.
					CALL	.SCAN_NEXT	; Retorna a pr�xima tecla.
					JR		NZ, 1b		; Se teve mais uma, processa
					
					LD		A, C		; Retorna qtd de teclas lidas
					DB		%00101110	; Instru��o LD L, nn : 00 101 110 - oculta a pr�xima instru��o sacrificando o L
.exitpopaf:			POP		BC			; Descarta o AF salvo acima, mas preservando o A.
					DB		%00101110	; Instru��o LD L, nn : 00 101 110 - oculta a pr�xima instru��o sacrificando o L
.NOKEYPRESSED:		XOR		A			; A em 0, no keys (tanto modo BASIC como GAME)
.exit:				POP		BC, DE, HL
					OR		A			; Ajusta Flags
					RET

.MODO_BASIC:		; Retorna EXASC.UNK em A, com tecla em KEY0.
					CALL	.SCAN		; SCAN retorna c�digo em L, ou com Z = 1 (A = 0) se n�o tiver nada pressionado.
					JR		Z, .NOKEYPRESSED
					LD		(IY), L
					LD		A, EXASC.UNK	; Coloca o retorno em A.
					JR		.exit
					
.SCAN:				; Retorna em L o ASCII da primeira tecla detectada, ou volta ao caller do caller se n�o tiver nada pressionado
					; Destr�i HL, DE, BC e AF
					
.SCAN_START:		; Se chegamos aqui, � porque a FASTKEYPRESSED? l� em cima disse que tinha tecla pressionada,
					; mas nada impede o azar desgra�ado do usu�rio soltar a tecla no exato milisegundo em que chegamos aqui!
					; Por isso o c�digo se previne de ser chamado sem nenhuma tecla pressionada.
					
					LD		BC, $FE00	; B vai shiftar de #FE (11111110) at� #7F (01111111) para selecionar cada "linha" do teclado.
										; C vai contar a quantidade de teclas simult�neas (0~GAME_KEYCOUNT]) (s� relevante em modo GAME)
					LD		D, 0		; D vai contar as linhas da matriz de teclado
.K1					LD		E, -1		; E as colunas do registro lido. Come�a em -1 por necessidade do algoritmo.
			
			; Seleciona linha do teclado indicada por B.

					LD		A, (HEAD)			; Previne debounce em modo GAME.
					INC		A
					JR		Z, 1f
					
					CALL	PSGIO.GET0FMASKED		; Modo GAME. Sem debounce.
					JR		.SCAN_NEXT					

1:					CALL	PSGIO.GETKEYBLINE		; L� o registro da linha selecionada por B em A.

.SCAN_NEXT:			; Esta (sub)rotina recome�a a busca de nova tecla a partir de onde foi encontrada a anterior.
					; Usada pelo modo GAME que l� at� <N> teclas simult�neas.
					; O Caller deve resguardar os valores de:
					;	A, DE, BC

; D vai variar de 0 a 7 para contar cada bit de cada linha da matriz do teclado
; E vai variar de 0 a 5 para contar as colunas da linha corrente.
.K3:				RRCA
					INC		E
					JR		NC, .XLAT_SCANCODE	; tecla pressionada! Converte em ASCII e retorna pro caller
					
					LD		L, A				; Salva A		
					LD		A, E
					CP		5					; Se estamos na 5� coluna, n�o h� mais o que fazer nesta linha.
					LD		A, L				; Restaura A
					JR		C, .K3
					
					; J� "parseamos" todo os 6 bits da linha corrente. O A n�o interessa mais.
					
					RLC		B					; Rolamos para a esquerda, para ativar a proxima linha
					INC		D					; Incrementa o n�mero da linha sendo scaneada
					LD		A, D
					CP		8					; Checamos se j� terminamos (s�o 8 linhas)
					JR		C, .K1

			; Acabaram-se as linhas e nada foi detectado.
					XOR		A					; Flag de fim de scan.
.SCAN_END:			OR		A					; Ajusta flag Zero
					RET

; Converte coordenada Linha D, Coluna E numa teclada baseada na tabela de convers�o selecionada em IX
; Retorna tecla lida em L
.XLAT_SCANCODE:
					PUSH 	AF			; Precisamos salvar A por causa do registro da linha corrente da matriz de teclado
					PUSH	DE			; Precisamos salvar DE por cauxa da coordenada y,x corrente na matrix
					PUSH	BC			; Precisamos salvar BC por causa da m�scara de sele��o de linha e do contador de teclas lidas

					LD		A, 6		; N� de teclas por linha
					LD		C, D		; D � o n�mero da linha no momento.
					CALL	_MPY		; HL <- A * C
					
					LD		D, 0		; O par DE agora contem o n�mero da coluna em 16 bits.
					ADD		HL, DE		; Adiciona � HL. Isto d� o �ndice do byte na tabela.
					
					LD		DE, IX		; Recupera o endere�o inicial da tabela de convers�o ativa.
					ADD		HL, DE		; Adiciona em HL.
					
					LD		A, (HL)		; Pronto! A agora tem o c�digo da tecla que reponde � coordenada D,E da matriz de teclado.

					LD		L, A		; Coloca o resultado em L
					POP		BC, DE, AF	; Restaura todo mundo
					JR		.SCAN_END	; e retorna

.END:

	ENDMODULE
	
KEYBRD_ORG_SIZE			EQU		$C464 - $C37F + 1 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
													; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
													; (tem margem pra isso, mas d� mais trabalho!)
KEYBRD_TEST_BED_END		EQU		$
KEYBRD_TEST_BED_SIZE		EQU		KEYBRD_TEST_BED_END - KEYBRD_TEST_BED_START
	DISPLAY	"new size ",/H,KEYBRD_TEST_BED_SIZE
	DISPLAY	"org size ",/H,KEYBRD_ORG_SIZE
	DISPLAY "KEYBRD code diferen�a ",/D,(KEYBRD_ORG_SIZE - KEYBRD_TEST_BED_SIZE)

KEYBOARD_SCAN_TBL_TEST_BED_START		EQU	$

; ======
; Look up table para converter scancodes em c�digo de tecla
KEYBOARD_SCAN_TBL:
.linesize			EQU 48
.nomod				EQU 0 * .linesize
						DB	       "@",       "H",       "P",       "X",       "0",       "8"		; Linha 0
						DB	       "A",       "I",       "Q",       "Y",       "1",       "9"		; Linha 1
						DB         "B",       "J",       "R",       "Z",       "2",       ":"		; Linha 2
						DB         "C",       "K",       "S",  ASCII.CR,       "3",       ";"		; Linha 3
						DB	       "D",       "L",       "T",       " ",       "4",       ","		; Linha 4
						DB         "E",       "M",       "U", ASCII.DEL,       "5",       "-"		; Linha 5
						DB         "F",       "N",       "V",       "^",       "6",       "."		; Linha 6
						DB         "G",       "O",       "W", EXASC.UNK,       "7",       "/"		; Linha 7
.shift				EQU	1 * .linesize
						DB	       "]",       "h",       "p",       "x",       "[",       "("		; Linha 0
						DB	       "a",       "i",       "q",       "y",       "!",       ")"		; Linha 1
						DB         "b",       "j",       "r",       "z",       '"',       "*"		; Linha 2
						DB         "c",       "k",       "s",      "\\",       "#",       "+"		; Linha 3
						DB	       "d",       "l",       "t",  ASCII.HT,       "$",       "<"		; Linha 4
						DB         "e",       "m",       "u", EXASC.INS,       "%",       "="		; Linha 5
						DB         "f",       "n",       "v",       "_",       "&",       ">"		; Linha 6
						DB         "g",       "o",       "w", EXASC.UNK,       "'",       "?"		; Linha 7
.control			EQU	2 * .linesize
						DB	 ASCII.NUL,  ASCII.BS, ASCII.DLE, ASCII.CAN, EXASC.F10,  EXASC.F_8		; Linha 0
						DB	 ASCII.SOH,  ASCII.HT, ASCII.DC1,  ASCII.EM, EXASC.F_1,  EXASC.F_9		; Linha 1
						DB   ASCII.STX,  ASCII.LF, ASCII.DC2, ASCII.SUB, EXASC.F_2,   ASCII.GS		; Linha 2
						DB   ASCII.ETX,  ASCII.VT, ASCII.DC3, EXASC.PAU, EXASC.F_3,   ASCII.FS		; Linha 3
						DB	 ASCII.EOT,  ASCII.FF, ASCII.DC4, EXASC.BRK, EXASC.F_4,   ASCII.RS		; Linha 4
						DB   ASCII.ENQ,  ASCII.CR, ASCII.NAK, EXASC.RUB, EXASC.F_5,  ASCII.ESC		; Linha 5
						DB   ASCII.ACK,  ASCII.SO, ASCII.SYN, EXASC.CAP, EXASC.F_6,   ASCII.US�		; Linha 6
						DB   ASCII.BEL,  ASCII.SI, ASCII.ETB, EXASC.UNK, EXASC.F_7,  EXASC.UNK		; Linha 7
.both				EQU	3 * .linesize  
						DB	  BASTK.TEXT,   BASTK.HOME,    BASTK.PRINT,    BASTK.NEXT,   BASTK.EDIT,  BASTK.COLUMN		; Linha 0
						DB	  BASTK.READ,  BASTK.INPUT,     BASTK.STOP,    BASTK.PLOT,    BASTK.NEW,    BASTK.AUTO		; Linha 1
						DB   BASTK.GOSUB,   BASTK.THEN,      BASTK.RUN,  BASTK.RETURN,   BASTK.EXIT,  BASTK.NORMAL		; Linha 2
						DB   BASTK.CLEAR,   BASTK.POKE,     BASTK.STEP,    BASTK.CALL,   BASTK.SLOW, BASTK.INVERSE		; Linha 3
						DB	  BASTK.DATA,   BASTK.LIST,    BASTK.TLOAD,     BASTK.REM,   BASTK.FAST,    BASTK.LEFT		; Linha 4
						DB    BASTK.PEEK,    BASTK.DIM,     BASTK.SAVE,   BASTK.TEMPO, BASTK.UNPLOT,    BASTK.DRAW		; Linha 5
						DB     BASTK.FOR,  BASTK.SOUND,  BASTK.RESTORE,   BASTK.COLOR, BASTK.UNDRAW,   BASTK.RIGHT		; Linha 6
						DB    BASTK.GOTO,   BASTK.CONT,     BASTK.LOAD,     EXASC.UNK,  BASTK.WIDTH,     BASTK.MID		; Linha 7

; ********** ( Source code Compatibility )
	
SKEY?				EQU	KEYBRD.CIN?		; Para compatibilizar c�digo fonte.

KEYBOARD_SCAN_TBL_ORG_SIZE			EQU		0	 	 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
														; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
														; (tem margem pra isso, mas d� mais trabalho!)
KEYBOARD_SCAN_TBL_TEST_BED_END		EQU		$
KEYBOARD_SCAN_TBL_TEST_BED_SIZE		EQU		KEYBOARD_SCAN_TBL_TEST_BED_END - KEYBOARD_SCAN_TBL_TEST_BED_START
	DISPLAY	"new size ",/H,KEYBOARD_SCAN_TBL_TEST_BED_SIZE
	DISPLAY	"org size ",/H,KEYBOARD_SCAN_TBL_ORG_SIZE
	DISPLAY "SKEY? data diferen�a ",/D,(KEYBOARD_SCAN_TBL_ORG_SIZE - KEYBOARD_SCAN_TBL_TEST_BED_SIZE)
