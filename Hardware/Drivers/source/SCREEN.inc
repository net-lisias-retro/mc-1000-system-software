; ==========
; SCRN : M�dulo Driver da Tela (40 e 80 colunas)
;
	MODULE SCRN

; =====
; INIT: TODO
;
INIT:	; Modo de texto.
				XOR		A
				LD		(CHRMSK), A

				CALL	MC6847.INIT
				CALL	MC6845.PROBE

			; Inicializa ao mesmo tempo COL80? e C40?80
				LD		A, COSW.COL40.INIT
				JR		NZ, .NO80
				CALL	MC6845.INIT
				LD		A, COSW.COL80.INIT
.NO80:			LD		(COSW), A

				LD		A, Z80.OP.JP
				LD		(JOBM), A
				LD		HL, FLASH_JOB
				LD		(JOBM+1), HL

; =====
; RESET: TODO
;
RESET:
				LD		HL, COSW
				BIT		COSW.C40?80, (HL)
				JP		Z, MC6847.RESET
				JP		MC6845.RESET

CLRMEM:		; Preenche a tela com BC espa�os
				LD		HL, (VIDEODEF.SNAM)

CLRHL:		; Preenche o buffer apontado por HL com BC espa�os
				LD		(HL), ' '
				LD		DE, HL
				INC		DE
				DEC		BC			; J� botamos um ali em cima!
				LDIR
				RET

BANK0:		; Desabilita VRAM.
				XOR		A
				OUT		(MC6845.IO.COL80), A
				LD		A, (MC6847.MODBUF)
				OR		#01
				OUT		(MC6847.IO.COL32), A
				RET

BANKV:		; Habilita a VRAM do VDG/CRTC ativo.
				LD		HL, COSW
				BIT		COSW.C40?80, (HL)
				JP		Z, MC6847.BANKV
				JP		MC6845.BANKV

; =====
; COUT: Imprime o caracter armazenado em C.
;
COUT:
				PUSH	HL, DE, BC, AF
				CALL	BANKV
				CALL	UPDBC

.CO0:	; Imprime o caracter armazenado em C (continua��o).
				LD		A, C

		; Trata as seq��ncias de ESCape
				LD		HL, COSW

				BIT		COSW.ESC.CURSOR?, (HL)
				JP		NZ, .DIRECTMODE

				BIT		COSW.ESC.LINE?, (HL)
				JP		NZ, .LINEMODE

				BIT		COSW.ESC.SCREEN?, (HL)
				JP		NZ, .SCREENMODE

				BIT		COSW.ESC?, (HL)
				JR		NZ, .ESCAPEMODE

		; N�o estamos em ESCape. Tratamos o caractere normalmente

				CP		ASCII.ESC
				JP		NZ, .ESCOUT
				SET		COSW.ESC?, (HL)		; Ativa um flag de seq��ncia de ESCape e aguarda pr�ximo caractere.
				JR		.SCOE

.ESCOUT:		LD		HL, (CURSOR.SNPTR)
				CP		ASCII.DEL 			; � RUBOUT?
				JR		NZ, 1f
				CALL	.DO_BS
				LD		(HL), ' '			; "Apaga" o caractere sob o cursor no v�deo. Cabe ao cliente mover o buffer dele e atualizar a tela depois.
				JP		.SCO0

1:				CP		' ' 				; � caracter de controle?
				JR		C, .CNTRLCH

.OTCH:	; Impress�o de caracter imprim�vel.
		; Coloca caracter na VRAM na posi��o do cursor.
				LD		A, (CHRMSK)
				XOR		C
				LD		(HL), A
.OTCH1:	; Incrementa posi��o horizontal do cursor.
				LD		A, (VIDEODEF.LLNG)
				LD		B, A
				LD		A, (CURSOR.X)
				INC		A
		; Zera se atingiu o fim da linha.
				CP		B
				JR		NZ, .OTCH0
				XOR		A
.OTCH0:			LD		(CURSOR.X), A
		; Incrementa posi��o do cursor na VRAM.
				INC		HL
		; Se atingiu fim da tela, providenciar .ROLL.
				PUSH	AF
				LD		DE, (VIDEODEF.ENAM)
				CALL	CP_HLDE
				JR		NZ, .SCO3

			; Impress�o chegou ao fim da tela.
				POP		AF
				CALL	.ROLLUP
				LD		HL, (CURSOR.LNHD)
				JR		.SCO2

.SCO3:			POP		AF
				CALL	Z, .INHD

.SCO2:	; Armazena posi��o atualizada do cursor na VRAM.
				LD		(CURSOR.SNPTR), HL

.SCO0:	; Anula seq��ncia de ESCape (se houver).
				LD		A, (COSW)				; Desliga todos os bits de COSW, exceto o que indicam status do video e cursor.
				AND		COSW.SAFEMASK
				LD		(COSW), A

.SCOE:	; Encerra impress�o.
				LD		HL, COSW				; Religa o cursor se ele estiver configurado para estar ativo.
				BIT		COSW.CURSOR?, (HL)
				CALL	NZ, UPDB

				CALL	BANK0
				POP		AF, BC, DE, HL
				RET

.ESCAPEMODE:	; Trata seq��ncias ESCape
				CP		'D'
				CALL	Z, .ROLLDOWN
				// Intentional fallthrough

				CP		'M'
				JR		NZ, 1f
				CALL	.ROLLUP
				JR		.SCO0

1:				LD		HL, COSW

				CP		'='
				JR		NZ, 1f
				SET		COSW.ESC.CURSOR?, (HL)
				JR		.SCOE

1:				CP		'['
				JR		NZ, 1f
				SET		COSW.ESC.LINE?, (HL)
				JR		.SCOE

1:				CP		'#'
				JR		NZ, 1f
				SET		COSW.ESC.SCREEN?, (HL)
				JR		.SCOE

1:				JP		.ESCOUT

.CNTRLCH:
		; Trata impress�o de caracter de controle.
				CP		ASCII.BEL ; � BEEP?
				JR		NZ, 1f
		; Impress�o de caracter de controle #07 (BEL).
.BELL:			CALL	_BEEP
				JP		.SCO0

1:				CP		ASCII.BS ; � BS?
				JR		NZ, 1f
.BS				CALL	.DO_BS
				JP		.SCO0

1:				CP		ASCII.FF 	; � avan�o de cursor?
				JR		NZ, 1f
		; Impress�o de caracter de controle #0C (FF).
.FF:
				LD		DE, (VIDEODEF.ENAM)
				DEC		DE
				CALL	@CP_HLDE
				JP		Z, .SCO0
				JP		.OTCH1

1:				CP		ASCII.LF 				; � LF?
				JR		NZ, 1f

		; Impress�o de caracter de controle #0A (LF).
.LF:		; Incrementa a posi��o do cursor na VRAM com a quantidade de caracteres de uma linha.
				LD		BC, (VIDEODEF.LLNG)
				ADD		HL, BC
			; Se exceder o final da tela, rola uma linha.
				LD		DE, (VIDEODEF.ENAM)
				LD		A, H
				CP		D
				JR		NZ, .LF0
				LD		A, L
				CP		E
				JR		NC, .LF1
			; Incrementa a posi��o de in�cio da linha com a quantidade de caracteres de uma linha.
.LF0:			EX		HL, DE			; Preserva HL, sacrificando DE
				LD		HL, (CURSOR.LNHD)
				ADD		HL, BC
				LD		(CURSOR.LNHD), HL
				EX		HL, DE			; "POP" HL
				JP		.SCO2
.LF1:
				CALL	.ROLLUP
				LD		HL, (CURSOR.SNPTR)	; Recarrega a posi��o original, pois na pr�tica o cursor n�o mudou de endere�o f�sico!
				JP		.SCO2

1:				CP		ASCII.CR 		; � CR?
				JR		NZ, 1f

		; Impress�o de caracter de controle #0D (CR).
		; Move o cursor para o in�cio da linha.
.CR:			LD		HL, (CURSOR.LNHD)
				LD		(CURSOR.SNPTR), HL
				XOR		A
				LD		(CURSOR.X), A
				JP		.SCO0

1:				CP		ASCII.VT 		; � sobe linha?
				JR		NZ, 1f

		; Impress�o de caracter de controle #0B.
			; Se o cursor j� est� na primeira linha apenas produz BEEP.
.VT:			LD		HL, (CURSOR.LNHD)
				LD		DE, (VIDEODEF.SNAM)
				CALL	@CP_HLDE
				CALL	Z, _BEEP
				JP		Z, .SCO0

			; Decrementa a posi��o de in�cio da linha com a quantidade de caracteres de uma linha.
				LD		DE, (VIDEODEF.LLNG)
				XOR		A						; Zera Carry
				SBC		HL, DE
				LD		(CURSOR.LNHD), HL

			; Decrementa a posi��o do cursor na VRAM com a quantidade de caracteres de uma linha.
				LD		HL, (CURSOR.SNPTR)
				XOR		A						; Zera Carry
				SBC		HL, DE
				JP		.SCO2

1:				CP		ASCII.SO 		; ShiftOut (letras reversas)?
				JR		NZ, 1f
				LD		A, $80
				LD		(CHRMSK), A
				JR		.OTCHEND

1:				CP		ASCII.SI 		; ShiftIn (letras normais)?
				JR		NZ, 1f
				XOR		A
				LD		(CHRMSK), A
				JR		.OTCHEND

1:				CP		ASCII.SUB 		; � limpa tela?
				JP		Z, .CLEAR

1:				CP		ASCII.RS 		; � in�cio de tela?
				JP		Z, .HOME

		; Se n�o � um caractere de controle reconhecido, ignore a impress�o consumindo o caractere sem fazer nada com ele.

.OTCHEND:		JP		.SCO0

.INHD:	; Atualiza endere�o do in�cio da linha atual.
				PUSH	HL
				LD		DE, (VIDEODEF.LLNG)
				LD		HL, (CURSOR.LNHD)
				ADD		HL, DE
				LD		(CURSOR.LNHD), HL
				POP		HL
				RET

; Muda modo de tela:
.SCREENMODE:
				LD		HL, COSW

				CP		'0'
				JR		NZ, 1f
				RES		COSW.CURSOR?, (HL)
				JR		3f

1:				CP		'1'
				JR		NZ, 1f
				SET		COSW.CURSOR?, (HL)
				JR		3f

1:				CP		'6'
				JR		NZ, 1f
				RES		COSW.C40?80, (HL)
				JR		2f

1:				CP		'5'
				JP		NZ, 3f
				SET		COSW.C40?80, (HL)
2:				CALL	SCRN.RESET

3:				JP		.SCO0

; Edi��o de linha:
.LINEMODE:
				CP		'K'
				JR		Z, .CLREOL

1:				CP		'J'
				JP		Z, .CLREOS

1:				CP		'L'
				CALL	Z, .LINEINS
				// Intentional fallthrough

1:				CP		'M'
				CALL	Z, .LINEDEL
				// Intentional fallthrough

2:				JP		.SCO0

; Endere�amento direto do Cursor
.DIRECTMODE:	; Trata seq��ncia de ESC=
.DT1:	; J� tratamos posi��o vertical?
				BIT		COSW.ESC.CURSOR.Y?, (HL)	; HL J� vem carregado com COSW
				JR		NZ, .DT2

		; N�o. Estamos tratando o vertical ent�o.
				LD		(CORD.Y), A
				SET		COSW.ESC.CURSOR.Y?, (HL)
				JP		.SCOE

.DT2:	; Tratamos posi��o horizontal.
				LD		(CORD.X), A

				LD		BC, (CORD)		; B <- .X ;  C <- .Y

		; Determina n�mero de linhas do modo texto.
				LD		HL, VIDEODEF.DEEP
				LD		A, C
			; Se a posi��o vertical excede o limite, ignora seq��ncia de escape.
				CP		[HL]
				JP		NC, .SCO0

		; Determina n�mero de colunas do modo texto.
				LD		HL, VIDEODEF.LLNG
				LD		A, B
			; Se a posi��o horizontal excede o limite, ignora seq��ncia de escape.
				CP		(HL)
				JP		NC, .SCO0

.ADDRESS:	; Recalcula vari�veis de posi��o do cursor.
				LD		DE, (VIDEODEF.LLNG)
				LD		A, C
				LD		(CURSOR.Y), A
				LD		C, B					; Preserva o B, que ser� destrupido abaixo
				CALL	FMATH.MPY.DExA			; Resultado em HL
				LD		DE, (VIDEODEF.SNAM)
				ADD		HL, DE
				LD		(CURSOR.LNHD), HL

				LD		B, 0					; C j� cont�m o antigo B (X). Zera o B para transformar num n�mero de 16 bits.
				ADD		HL, BC
				LD		A, C
				LD		(CURSOR.X), A

				JP		.SCO2

.HOME:	; P�e o cursor no in�cio da tela.
				XOR		A							; Zera coluna/Linha atual do cursor na linha.
				LD		(SCRN.CURSOR.Y), A
				LD		(SCRN.CURSOR.X), A

				LD		HL, (VIDEODEF.SNAM)
				LD		(SCRN.CURSOR.LNHD), HL		; Posi��o do in�cio da linha atual do cursor na VRAM.

				JP		.SCO2

.CLEAR:	; Limpa tela.
				LD		A, (COSW)
				AND		COSW.C40?80.MSK			; Usamos m�scara para n�o mexer no HL.
				JR		NZ, .CLERS8

			; Para 32 colunas.
				CALL	MC6847.CLR
				JR		1f

.CLERS8:	; Para 80 colunas.
				CALL	MC6845.CLR
1:				JP		.SCO0

.CLREOS:	; Limpado cursor at� o fim da tela
				LD		HL, (VIDEODEF.ENAM)		; Pega o endere�o do fim da VRAM
				JR		.endcalc

.CLREOL:	; Limpa do cursor at� o fim da linha
				LD		HL,	(CURSOR.LNHD)		; Pega o endere�o da linha atual do cursor
				LD		DE, (VIDEODEF.LLNG)
				ADD		HL, DE					; Calcula o endere�o do in�cio da pr�xima linha
.endcalc:		XOR		A
				LD		DE, (CURSOR.SNPTR)
				SBC		HL, DE					; Subtraindo da posi��o corrente, temos quantos caracteres a serem apagados
				LD		BC, HL
				LD		HL, DE
				CALL	CLRHL
				JP		.SCO0

.LINEDEL:	; Deleta a linha corrente, rolando para cima.
			; Na lata, faz um ROLLUP mas a partir da linha corrente, ao inv�s do come�o da tela.
				LD		HL, (VIDEODEF.ENAM)
				LD		DE, (CURSOR.LNHD)
				XOR		A					; Zera A, zera Carry
				SBC		HL, DE
				LD		BC, HL
				LD		HL, DE
				JR		.ROLLUP1

.ROLLUP:	; Rola a tela de texto uma linha para cima.
				LD		HL, (VIDEODEF.SNAM)
				LD		BC, (VIDEODEF.ZNAM)
.ROLLUP1:
				PUSH	HL
				LD		DE, (VIDEODEF.LLNG)
				ADD		HL, DE
				POP		DE

.ROLLUP4:		LDIR
				LD		HL, (VIDEODEF.ENAM)
				LD		BC, (VIDEODEF.LLNG)
				SBC		HL, BC
				JP		CLRHL

.LINEINS:	; Insere uma linha em branco na linha corrente, rolando para baixo.
			; Na lata, faz um ROLLDOWN mas a partir da linha corrente, ao inv�s do come�o da tela.
				LD		HL, (VIDEODEF.ENAM)
				PUSH	HL
				LD		DE, (CURSOR.LNHD)
				XOR		A					; Zera A, zera Carry
				SBC		HL, DE
				LD		BC, HL
				POP		HL

				CALL	.do_roll_and_loadbc

				LD		HL, (CURSOR.LNHD)
				JP		CLRHL

.ROLLDOWN:	; Rola a tela de texto uma linha para baixo.
				LD		BC, (VIDEODEF.ZNAM)
				LD		HL, (VIDEODEF.ENAM)

				CALL	.do_roll_and_loadbc

				LD		HL, (VIDEODEF.SNAM)
				JP		CLRHL

.do_roll_and_loadbc:
				DEC		HL

				PUSH	HL
				LD		DE, (VIDEODEF.LLNG)
				XOR		A					; Zera A, zera Carry
				SBC		HL, DE
				POP		DE

				LDDR
				LD		BC, (VIDEODEF.LLNG)
				RET

	; Impress�o de caracter de controle #08 (BS).
	; Se encontrou o in�cio da tela, produz BEEP.
.DO_BS:
				LD		DE, (VIDEODEF.SNAM)
				LD		HL, (CURSOR.SNPTR)
				CALL	@CP_HLDE
				CALL	Z, _BEEP
				JP		Z, .SCO0
				LD		HL, (CURSOR.SNPTR)
				DEC		HL
				LD		(CURSOR.SNPTR), HL

.RUBOT1:	; Decrementa posi��o horizontal do cursor.
				LD		A, (CURSOR.X)				; Checa se estamos na primeira coluna da linha
				OR		A
				JR		NZ, 1f

			; Se primeira coluna, p�e o cursor no fim da linha anterior.
				LD		HL, (CURSOR.LNHD)
				LD		BC, (VIDEODEF.LLNG)
				XOR		A
				LD		B, A
				SBC		HL, BC
				LD		(CURSOR.LNHD), HL
				LD		A, (VIDEODEF.LLNG)

1:				DEC		A
				LD		(CURSOR.X), A
				RET

UPDB:	; Atualiza status do Cursor
				LD		HL, COSW
				BIT		COSW.C40?80, (HL)
				JP		Z, MC6847.UPDB
				JP		MC6845.UPDB

UPDBC:	; Desliga o Cursor
				LD		HL, COSW
				BIT		COSW.C40?80, (HL)
				JP		Z, MC6847.UPDBC
				JP		MC6845.UPDBC

; Decrementa contador para intermit�ncia do cursor.
FLASH_JOB:		LD		A, (CURSOR.FLASHB)
				DEC		A
				LD		(CURSOR.FLASHB), A
				RET		NZ

; Efetua intermit�ncia do cursor e registra seu estado atual.
				PUSH	HL
				CALL	UPDB
				POP		HL
				RET

	ENDMODULE
