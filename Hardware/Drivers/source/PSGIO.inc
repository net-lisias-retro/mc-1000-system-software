GETKEYBLINE_TEST_BED_START		EQU		$

; ==========
; PSG: M�dulo Driver do PSG.
	MODULE	PSGIO

; ======
; PSG.GETKEYBLINE: L� em A a linha especificada pela m�scara em B com debounce de 7ms.
; Destr�i:
;		AF, IFF
; Retorno:
;		A: M�scara l�gica inversa com as teclas pressionadas na linha selecionada
;			Os bits s�o normalmente ligados.
;			Pressionar uma tecla zera o bit correspondente. O bit 6 � sempre SHIFT e o bit 7 � sempre CTRL.
GETKEYBLINE:
				PUSH	BC, HL
				AND		A				; Zera Carry 
				CALL	GET0FMASKED		; Leitura em A. (Agora o retorno ocorre com as interrup��es desligadas)

				; Debounce: A leitura deve ficar est�vel por 7 ms, do contr�rio a rotina n�o aceita o INPUT e tenta de novo
.again:			LD		C, 8			; Loop de 7 intera��es (acaba em 0)	
				LD		L, A			; Salva primeira leitura em L
				
.loop:			PUSH	AF, BC
				LD		BC, 1			; Delay de 1 ms
				CALL	_DELAYB
				POP		BC, AF
				
				DEC		C				; Acabou?
				JR		Z, .exit		; Se sim, A possui o valor (est�vel) lido do teclado
				
				IN		A, (RD)			; L� de novo
				CP		L				; V� se � igual � ultima leitura
				JR		Z, .loop		; Se sim, vamos ao resto do debounce
				JR		.again			; Se n�o, come�a tudo de novo.
				
.exit:			POP		HL, BC
				EI
				RET

; ======
; PSG.GET0FMASKED: L� em A a linha especificada pela m�scara em B. Se Carry = 0, n�o reativa as interrup��es.
; Destr�i:
;		AF, IFF
; Retorno:
;		A: M�scara l�gica inversa com as teclas pressionadas na linha selecionada
;			Os bits s�o normalmente ligados.
;			Pressionar uma tecla zera o bit correspondente. O bit 6 � sempre SHIFT e o bit 7 � sempre CTRL.
GET0FMASKED:				
				DI					; Impede que a interrup��o peri�dica do Z80 (0x0038) que
									; por padr�o � configurada para chamar a rotina de som (INTRUP)
									; se intrometa e altere a sele��o dos registradores do PSG.
				LD		A, $0E
				OUT		(REG), A
				LD		A, B
				OUT		(WR), A
		; Intentional fallthrough !			
				DB		%00111110	; Instru��o LD A, nn : 00 111 110 - oculta a pr�xima instru��o sacrificando o A

; ======
; PSG.GET0F: L� em A o registrador #0F do PSG. (INPUT). Sempre reativa as interrup��es.
; Idealmente, ficar� no mesmo endere�o da GETPSG0F original para garantir retrocompatibilidade
; Destr�i:
;		AF, IFF
; Retorno:
;		A:
;			Os bits s�o normalmente ligados.
;			Pressionar uma tecla zera o bit correspondente. O bit 6 � sempre SHIFT e o bit 7 � sempre CTRL.
GET0F_LEGACY:
				SCF					; For�a interrup��es ativas ao retornar.
; ======
; PSG.GET0F: L� em A o registrador #0F do PSG. (INPUT). Se Carry = 0, n�o reativa as interrup��es.
; Destr�i:
;		AF, IFF
; Retorno:
;		A:
;			Os bits s�o normalmente ligados.
;			Pressionar uma tecla zera o bit correspondente. O bit 6 � sempre SHIFT e o bit 7 � sempre CTRL.
GET0F:
				DI					; Impede que a interrup��o peri�dica do Z80 (0x0038) que
									; por padr�o � configurada para chamar a rotina de som (INTRUP)
									; se intrometa e altere a sele��o dos registradores do PSG.
				LD		A, $0F
				OUT		(REG), A
				IN		A, (RD)
				RET		NC
				EI
				RET

	ENDMODULE
	
GETKEYBLINE_ORG_SIZE			EQU		$C46E - $C465 	; O ideal � que a rotina nova n�o ultrapasse o espa�o da antiga
														; sen�o vou ter que refatorar um peda�o da ROM para encaix�-la
														; (tem margem pra isso, mas d� mais trabalho!)
GETKEYBLINE_TEST_BED_END		EQU		$
GETKEYBLINE_TEST_BED_SIZE		EQU		GETKEYBLINE_TEST_BED_END - GETKEYBLINE_TEST_BED_START
	DISPLAY	"new size ",/H,GETKEYBLINE_TEST_BED_SIZE
	DISPLAY	"org size ",/H,GETKEYBLINE_ORG_SIZE
	DISPLAY "GETKEYBLINE diferen�a ",/D,(GETKEYBLINE_ORG_SIZE - GETKEYBLINE_TEST_BED_SIZE)



; *********
; Source code Compatibility

GETPSG0F		EQU		PSGIO.GET0F_LEGACY
