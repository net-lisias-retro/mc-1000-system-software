	OUTPUT	bin/VRAM16.BAS
	ORG	$0000

NULL		EQU		0
RAMBYTE		EQU		$A5
VRAMBYTE	EQU		$5A
	
COL32		EQU		$80

MODBUF		EQU		$00F5
	
SYS.COUT	EQU		$C00C
SYS.CIN		EQU		$C006
SYS.MSG		EQU		$C018
SYS.KEY		EQU		$C31B
SYS.GETL	EQU		$C01E
SYS.ISCN	EQU		$C021

DEBUG		EQU		$F1F4
PRNTHL		EQU		$EE8B
	
FILEHEADER:
.filename:	DB		"VRM16"
			DB		$0D
.start		DW		BASTEXT
.end		DW		LOADER + LOADER.size + PAYLOAD_size

BASTEXT		EQU		$03D5
BINTARGET	EQU		$2000
	
			TEXTAREA		BASTEXT
PROGRAM:	
			; Programa BASIC portador do programa em c�digo de m�quina:
			; 1 CLEAR 512,<mem> : CALL 1004
		
			; 1� registro de linha.
			DW		.linha2		; Endere�o do pr�ximo registro de linha.
			DW		1			; N�mero da linha.
			DB		0xa9		; Token de CLEAR.
			DB		"512, 8192"
			DB		":"
			DB		0xa2		; Token de CALL.
			DB		"1004"		; 0x0e3c, endere�o do LOADER. TODO: Trocar por uma Macro que gere o ascii
			DB		0x00		; Fim da linha.
		
			; 2� registro de linha.
.linha2		DW	0x0000			; Indica fim do programa.
	
LOADER:	
			LD	HL, PAYLOAD
			LD	DE, PAYLOAD_start
			LD	BC, PAYLOAD_size
			LDIR
			JP	PAYLOAD_start
.size		EQU		$-LOADER	

PAYLOAD:
	ENDT
	
			TEXTAREA	BINTARGET
PAYLOAD_start:
			CALL	SYS.ISCN
			
	; Mensagem com ANYKEY
			LD		HL, str_mensagem
			CALL	SYS.MSG
			CALL	SYS.KEY
						
			DI		; Desabilita as interrup��es.
			
	; Limpa a RAM toda, para facilitar diagn�stico quando as coisas d�o errado.
			LD		HL, $6000
			LD		DE, $6001
			LD		BC, $8000-$6000-1
			LD		(HL), 0
			LDIR
	
	; Seta a VRAM
			CALL	VRAM_ON			 
			CALL	SETMEMPAGE
			
	; Faz uma c�pia da VRAM em $6000 para debugging
			CALL	VRAM_ON			 
			LD		HL, $8000
			LD		DE, $6000
			LD		BC, $2000
			LDIR

	; Checando o tamanho da VRAM
			LD		HL, $8000
			LD		BC, $2002	; 8Kb + 2 bytes
			
			CALL	VRAM_ON
1:			LD		A, (HL)		; L� byte da VRAM
			CP		H			; Checa com o valor que colocamos l�
			JR		NZ, 2f		; Se na� for igual, fim
				
			INC		HL			; Se n�o, incrementa HL
			DEC		BC			; Decrementa BC
			LD		A, B
			OR		C
			JR		NZ, 1b		; If BC is not zero, next byte
			
		; Se chegamos aqui, acabou a VRAM e n�o detectamos! Isso n�o deveria acontecer!
			LD		HL, str_fault
			JR		PANIC
2:
	; Mensagem de resultados
			PUSH	HL				; Salva HL (primeiro endere�o cuja compara��o deu igual)
			CALL	SYS.ISCN
			LD		HL, str_result
			CALL	SYS.MSG
			POP		HL				; Resgata resultado da compara��o
			CALL	PRNTHL			; Imprime usado o BASIC
			LD		HL, str_newline
			CALL	SYS.MSG
			CALL	SYS.MSG
			
	; DEBUG para encerrar e facilitar checar mem�ria.
			CALL	VRAM_OFF
			EI
FIM:		JP		DEBUG
PANIC:		PUSH	HL
			CALL	SYS.ISCN
			LD		HL, str_panic
			CALL	SYS.MSG
			POP		HL
			CALL	SYS.MSG
			JR		FIM

SETMEMPAGE:
			LD		HL, $8000
			LD		BC, 8192
1:			LD		A, H
			LD		(HL), A
			INC		HL
			DEC		BC
			LD		A, B
			OR		C
			JR		NZ, 1b		
			RET

VRAM_ON:
			PUSH	AF
			LD      A, (MODBUF)
			AND     $FE
			OUT     (COL32), A
			POP		AF
			RET
			
VRAM_OFF:
			PUSH	AF
			LD      A, (MODBUF)
			OR		$01
			OUT     (COL32), A
			POP		AF
			RET

;						-12345678901234567890123456789012
str_mensagem:  		DB	"PREPARANDO [$8000..$9FFF]\N\R"
					DB	"NA RAM E NA VRAM PARA TESTE.\N\R\N\R"
					DB	"A TELA FICARA CORROMPIDA POR\N\R"
					DB	"ALGUNS INSTANTES.\N\R"
					DB	"<ANY KEY>"
str_newline:		DB	"\R\N",NULL
str_result:			DB	"FIM DA VRAM EM : ",NULL
str_panic:			DB	"PANIC!!!\N\R",NULL
str_fault:			DB	"OVERFLOW DA VRAM!!\N\R",NULL

PAYLOAD_size	EQU		$ - PAYLOAD_start
	ENDT
