# Hardware :: Expansão (ainda mais simples) para 8Kb #

Esta seção trata da expansão para 8Kb da plaquinha de VRAM do MC-1000, ao invés de modificar a placa-mãe.

## Conteúdo ##

* [Binários](bin) prontos para usar
* [Documentação](docs/Documentação.md)
* [Código Fonte](source) dos utilitários de teste para quem quiser brincar com o código e/ou gerar seus príprios binários.

## Build Instructions ##

1. Ter todos os softwares mencionados na página principal compilados, instalados e no path da linha de comando.
1. Clonar este repositório e dar checkout do branch **master**
1. Abrir uma janela de terminal (ou CMD.EXE), e setar o diretório corrente para `<gitroot>/system-software/Hardware/VÍDEO/Expansão8Kb`
1. executar `make` ou `gmake` para atualizar os binários cujos fontes tenham sido modificados.`make clean` para limpar todos os binários e recompilar tudo do zero.
1. Ver documentação para detalhes dos arquivos gerados
