# MOD VRAM +2Kb.

Enquanto analisava os circuitos no esquemático do MC-1000 (disponível no Datasssete) eu notei uma discrepância:

> **O esquemático da plaquinha da VRAM não corresponde ao Layout da mesma!!!**

Desmontando o meu MC-1000 e analisando a plaquinha, eu notei que ela segue o Layout, e não o Esquemático. Curioso!

Com o MOD de VRAM com SRAM de 8Kb, eu já sabia que havia pinout no conector para drivear 8Kb e então, no esquemático, aquele pino flutuando era de certeza o pino de endereçamento dos 2Kb extras. Análise do Esquemático da placa mãe confirma a hipótese, e demonstrou que não é necessário executar o [MOD da SRAM de 8Kb](https://sites.google.com/site/ccemc1000/hardware/mods) para que aquele pino atuasse (não estou dizendo que o mod não é necessário para o chip de 8Kb, isso é outra solução)!
![](MC-1000_ModVRAM_Esquema.png)

Analisando o Layout (e a placa física), eu notei que justamente aquele pino que no Esquemático estava flutuando, na plaquinha era usado para drivear o Chip Select de um 4ª IC de 2Kb, que por sinal tinha seu espaço (e ilhas) reservado! 
![](MC-1000_ModVRAM_Layout.png)

Vendo o datasheet do D446C-2, ele é uma SRAM de 2Kb com tempo de resposta de 200ns. Vendo o datasheet do 6116-7 (uma SRAM de 2Kb bem mais fácil de encontrar por aqui), eu vejo que ele é pino compatível com o D446C e o que tem tempo de acesso de 70ns, então é compatível.

O MOD, então, é meramente soldar um capacitor de 104Z e um soquete de 24 pinos (ou soldar a SRAM diretamente, mas eu preferi soquetar) nas ilhas reservadas. Ver o link no Google Photos abaixo para detalhes de execução.

Executado o MOD, é necessário testá-lo. Para isso foram feitos dois programas em BASIC: um para usar no MC-1000 *stock* de 16K (bin/VRAM16.BAS), outro para usar no expandido para 64K (bin/VRAM64.BAS). Este último checa contra o problema da [expansão EM-1000](https://sites.google.com/site/ccemc1000/hardware/perifericos) que vaza as gravações à VRAM para a DRAM (e que não ocorre no MOD de expansão interna, ou no BlueDrive)

Quando executados em uma máquina padrão com VRAM de 6Kb, os programas retornam o valor 38912 na tela de resultado.

Quando executados em uma máquina com VRAM expandida (seja ele qual for), retornam o valor de 40960.

Um chip defeituoso vai retornar um valor diferente dos acima - neste caso, o endereço vai entregar o chip defeituoso:

* se for < 34816, foi o IC 1;
* se for >= 34816 e < 36864, o IC 2;
* se for >= 36864 e < 38912, o IC 3;
* se for >= 38912 e < 40960, o IC4 (se presente).

Os programas não dependem da BlueDrive (funcionam via tape também).

Para tanto, estão disponíveis binários em três formatos:

* .BAS - para ser copiado em um pendrive e usado no BlueDrive
* .cas - para ser usado em emuladores ou carregado no MC-1000 usando outra solução de memória de massa no futuro
* .wav - arquivo de som pronto para ser usado em *media players* para carregamento pela porta cassete.

As fotos da presepada estão em no meu [Google Photos](https://photos.google.com/share/AF1QipNaZ3bPaM3AaIEBCXHUZkUNQ_tO1hUrRwZKPDk3hsR8qPMaJkYB8WQnATkXuv81UA?key=cXZVUENjNUhDdWhBTGxMQ3c3NW12dTdQaE9KNV93).

	Lisias
	retro@lisias.net
	Abril, 2016
