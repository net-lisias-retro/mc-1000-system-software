# Bugs Corrigigos e Otimiza��es 

Nesta altura do campeonato, ficou evidente que o *firmware* do MC-1000 � uma colcha de retalhos que foi cerzida � v�rias m�os.

Al�m do BASIC 8K, originalmente da Microsoft, o Monitor foi constru�do a partir de rotinas de varios fornecedores. Muitas rotinas possuem "v�cios" vindos do 8080, que possui menos instru��es (e, portanto, o c�digo � otimizado de outra forma) que o Z80.

Aqui se documenta as altera��es n�o funcionais das rotinas.

## Bugs

**Work In Progress**

## Otimiza��es

### JP (HL)

Onde no c�digo existia:

	PUSH	HL
	LD		HL, (UPDBCM)
	EX		(SP), HL
	RET	

simplesmente troquei por

	JP		(UPDBCM)

Mesmo efeito, menos bytes, menos ciclos!

N�o fa�o a menor id�ia da raz�o de existir destes trechos de c�digo, o 8080 possui a instru��o `JP (addr)`. Eu especulo que este trecho de c�digo tenha vindo de uma biblioteca feita originalmente para o 8085, que at� onde eu conhe�o n�o possui jmp indireto mas possui as instru��es equilentes �s acima.

### JR Z, label ; CALL label2 ; label: blablaabl

Por exemplo:

				JR		NZ, UPDC40
				CALL	UPDB4				
	UPDC40:		

virou:

				CALL	Z, UPDB4				

### DL DE, (addr)

				LD		HL, (DLNG)
				EX		DE, HL
				LD		HL, (LNHD)

para

				LD		DE, (DLNG)
				LD		HL, (LNHD)

N�o se perde nem ganha nada - o `LD DE, (addr)` custa 4 bytes e 20 ciclos, ao passo que `LD HL, (addr) ; EX DE, HL` juntos custam a mesma coisa. Mas se eu tenho uma m�quina Z80, ent�o eu quero programas em Z80. :-)

Por outro lado, 

				EX		DE, HL
				LD		HL, (DENAM)
				EX		DE, HL

para

				LD		DE, (DENAM)

J� temos um ganho de 4 ciclos e um byte evitando o segundo EX. 
